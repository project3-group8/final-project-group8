import React, { useEffect } from 'react';
import './App.css';

import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from './redux/store';
import { Switch, Route } from 'react-router-dom';

import PrivateRoute from './components/PrivateRoute';
import LoginPage from "./pages/LoginPage";
import HomePage from "./pages/HomePage";
import RegisterPage from './pages/RegisterPage';
import ItemForm from "./components/ItemForm";
import GroupPage from './pages/GroupPage';
import { restoreLoginThunk } from './redux/auth/thunks';
import { setGroupId } from './redux/items/actions';
import ChartPage from './pages/ChartPage';
import NotifyPage from './pages/NotifyPage';
import NotFoundPage from './pages/NotFoundPage';

function App() {
  const dispatch = useDispatch();
  const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated);
  // console.log(isAuthenticated);

  const groupIdInLocalStorage = localStorage.getItem('group_id');     // avoid lost of group_id in page refresh
  if (groupIdInLocalStorage) {
    dispatch(setGroupId(Number(groupIdInLocalStorage)));
  }
  // console.log("group_id in localStorage:", groupIdInLocalStorage);
  // console.log("group_id in localStorage (number):", Number(groupIdInLocalStorage));

  useEffect(() => {
    if (isAuthenticated === null) {
      // restore login
      dispatch(restoreLoginThunk());
    }
  }, [dispatch, isAuthenticated]);

  return (
    <div className="App">
      {/* {isAuthenticated === null && <h1>loading...</h1>} */}
      {isAuthenticated !== null && (
        <Switch>
          <PrivateRoute path="/gp/:id" exact={true} component={GroupPage} />
          <PrivateRoute path="/inputItem" exact={true} component={ItemForm} />
          <Route path="/reg" exact={true} component={RegisterPage} />
          <Route path="/login" exact={true} component={LoginPage} />
          <PrivateRoute path="/" exact={true} component={HomePage} />
          <PrivateRoute path="/chart" exact={true} component={ChartPage} />
          <PrivateRoute path="/notify" exact={true} component={NotifyPage} />
          <Route component={NotFoundPage}/>
        </Switch>
      )}
    </div>
  );
}

export default App;
