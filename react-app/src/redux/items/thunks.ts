import { Dispatch } from 'redux';
import { IListItemProps } from '../../components/ItemsListItem';
import { loginFail } from '../auth/actions';
import { deleteItemFail, deleteItemSuccess, setCategories, setExpiryItems, setGroupId, setIsProcessing, setListItems, setShortageItems, updateQtyItemFail, updateQtyItemSuccess } from './actions';

const { REACT_APP_API_SERVER } = process.env;

export const getListItemsThunk = () => {
    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/items`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            dispatch(setListItems(data.listItems));
        }

    };

}

export const getChartItemsByGroupThunk = (group_id: number) => {
    return async (dispatch: Dispatch) => {

        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/items/getChartItemsByGroup`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                group_id,
            }),
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            // dispatch(setShortageItems(data.shortageItems));
            // console.log('ChartItems:', data.shortageItems);
        }

    };
}

export const getShortageItemsByGroupThunk = (group_id: number, alert_quantity: number) => {
    return async (dispatch: Dispatch) => {

        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/items/getShortageItemsByGroup`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                group_id,
                alert_quantity,
            }),
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            dispatch(setShortageItems(data.shortageItems));
            // console.log('shortageItems:', data.shortageItems);
        }

    };

}

export const getExpiryItemsByGroupThunk = (group_id: number, day_before: number) => {
    return async (dispatch: Dispatch) => {

        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/items/getExpiryItemsByGroup`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                group_id,
                day_before,
            }),
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            dispatch(setExpiryItems(data.expiryItems));
            // console.log('expiryItems:', data.expiryItems);
        }

    };

}


export const getSearchItemsByUserThunk = (key_word: string) => {
    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/items/getSearchItemsByUser`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                key_word,
            }),
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            dispatch(setListItems(data.listItems));
        }
    }
}





export const getSearchItemsByGroupThunk = (group_id: number, key_word: string) => {
    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/items/getSearchItemsByGroup`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                group_id,
                key_word,
            }),
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            dispatch(setListItems(data.listItems));
        }
    }
}

export const getSearchItemsByGroupByCatThunk = (group_id: number, key_word: string, category: string) => {
    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/items/getSearchItemsByGroupByCat`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                group_id,
                key_word,
                category,
            }),
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            dispatch(setListItems(data.listItems));
        }
    }
}

export const getListItemsByUserByGroupByCatThunk = (group_id: number, category: string) => {
    return async (dispatch: Dispatch) => {
        // console.log("In getListItemsByUserByGroupByCatThunk");
        // console.log("group_id: ", group_id);
        // console.log("category:", category);

        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
            return;
        }
        dispatch(setListItems([]));
        const res = await fetch(`${REACT_APP_API_SERVER}/items/getListItemsByUserByGroupByCat`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                group_id,
                category,
            }),
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            // console.log(data)
            dispatch(setListItems(data.listItems));
        }

    };

}

export const getCategoriesByUserByGroupThunk = (group_id: number) => {
    return async (dispatch: Dispatch) => {
        // console.log("In getCategoriesByUserByGroup");
        // console.log("group_id:", group_id);
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/items/getCategoriesByUserByGroup`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                group_id,
            }),
        });
        // console.log("After fetch in getCategoriesByUserByGroup");
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            dispatch(setCategories(data.categories));
        }

    };

}

export const deleteItemThunk = (props: IListItemProps) => {
    return async (dispatch: Dispatch) => {
        // console.log("In del thunk");

        const token = localStorage.getItem('token');

        if (!token) {
            dispatch(loginFail(''));
        }
        dispatch(setIsProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/items/deleteItemAndInvAndTrx`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                item_id: props.item_id,
                inventory_id: props.inventory_id
            }),
        });

        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const message = await res.json();
            // console.log(message);
            dispatch(deleteItemSuccess(props.index_value));
        } else {
            // console.log("delete fail")
            dispatch(deleteItemFail());
        }

    };

}

interface IUpdatedData {
    index_value: number;
    item_id: number;
    product: string;
    category: string;
    user_id: number;
    group_id: number;
    expiry_date: Date;
    quantity: number;
    image: string;
    inventory_id: number;
    updated_quantity: number;
}

export const updateQtyItemThunk = (updatedData: IUpdatedData) => {
    return async (dispatch: Dispatch) => {
        // console.log("In del thunk");

        const token = localStorage.getItem('token');

        if (!token) {
            dispatch(loginFail(''));
        }
        dispatch(setIsProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/items/updateQtyItemAndInvAndTrx`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                item_id: updatedData.item_id,
                inventory_id: updatedData.inventory_id,
                quantity: updatedData.quantity,
                updated_quantity: updatedData.updated_quantity,
            }),
        });

        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const message = await res.json();
            // console.log(message);
            dispatch(updateQtyItemSuccess(updatedData.index_value, updatedData.quantity));
        } else {
            // console.log("delete fail")
            dispatch(updateQtyItemFail());
        }

    };

}


export function getDefaultGroupByTokenThunk() {
    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/users/getDefaultGroupByToken`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            // console.log("getDefaultGroupByTokenThunk data:", data);

            // const group_id = localStorage.getItem('group_id');
            // if (!group_id) {
            localStorage.setItem('group_name', data.group.group_name);
            localStorage.setItem('group_id', data.group.group_id.toString());
            dispatch(setGroupId(data.group.group_id)); // Set the DEFAULT group id ONLY if not found in localStorage
            // }
        } else {
            console.log("getDefaultGroupByTokenThunk failed")
        }
    }
}
