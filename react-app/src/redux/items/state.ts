export interface IListItem {
    item_id: number;
    product: string;
    category: string;
    user_id: number;
    group_id: number;
    expiry_date: Date;
    quantity: number;
    image: string;
    inventory_id: number;
}

export interface ICategoryObj {
    category: string;
}

export interface IShortageItem {
    item_id: number;
    product: string;
    category: string;
    sum: string;    // Pls note that he data type of sum() return from "SELECT sum() FROM table GROUP BY ..." is string!!
}

export interface IExpiryItem {
    item_id: number;
    product: string;
    category: string;
    expiry_date: Date;
    quantity: number;
}


export interface IItemsState {
    isProcessing: boolean;
    shortageItems: Array<IShortageItem>;
    expiryItems: Array<IExpiryItem>;
    group_id: number;
    categoryObj: ICategoryObj;
    categories: Array<ICategoryObj>;
    listItems: Array<IListItem>;
}

export const initItemsState: IItemsState = {
    isProcessing: false,
    shortageItems: [] as Array<IShortageItem>,
    expiryItems: [] as Array<IExpiryItem>,
    group_id: 0,
    categoryObj: {category: 'All'},
    categories: [] as Array<ICategoryObj>,
    listItems: [] as Array<IListItem>,
    // 
};
