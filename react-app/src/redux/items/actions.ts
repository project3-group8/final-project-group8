import { ICategoryObj, IExpiryItem, IListItem, IShortageItem } from './state';

export const SET_LIST_ITEMS = "@@items/SET_LIST_ITEMS";
export const SET_CATEGORIES = "@@items/SET_CATEGORIES";
export const SET_CATEGORY_OBJ = "@@items/SET_CATEGORY_OBJ";
export const SET_GROUP_ID = "@@items/SET_GROUP_ID";
export const SET_SHORTAGE_ITEMS = "@@items/SET_SHORTAGE_ITEMS";
export const SET_EXPIRY_ITEMS = "@@items/SET_EXPIRY_ITEMS";

export const SET_IS_PROCESSING = "@@items/SET_IS_PROCESSING";
export const DELETE_ITEM_SUCCESS = "@@items/DELETE_ITEM_SUCCESS";
export const DELETE_ITEM_FAIL = "@@items/DELETE_ITEM_FAIL";

export const UPDATE_QTY_ITEM_SUCCESS = "@@items/UPDATE_QTY_ITEM_SUCCESS";
export const UPDATE_QTY_ITEM_FAIL = "@@items/UPDATE_QTY_ITEM_FAIL";

interface ISetListItems {
    type: typeof SET_LIST_ITEMS;
    listItems: Array<IListItem>;
}

interface ISetCategories {
    type: typeof SET_CATEGORIES;
    categories: Array<ICategoryObj>;
}

interface ISetCategoryObj {
    type: typeof SET_CATEGORY_OBJ;
    categoryObj: ICategoryObj;
}

interface ISetGroupId {
    type: typeof SET_GROUP_ID;
    group_id: number;
}

interface ISetShortageItems {
    type: typeof SET_SHORTAGE_ITEMS;
    shortageItems: Array<IShortageItem>;
}

interface ISetExpiryItems {
    type: typeof SET_EXPIRY_ITEMS;
    expiryItems: Array<IExpiryItem>;
}

interface ISetIsProcessing {
    type: typeof SET_IS_PROCESSING;
}

interface IDeleteItemSuccess {
    type: typeof DELETE_ITEM_SUCCESS;
    idx: number;    // the index of deleted item in listItems array
}

interface IDeleteItemFail {
    type: typeof DELETE_ITEM_FAIL;
}

interface IUpdateQtyItemSuccess {
    type: typeof UPDATE_QTY_ITEM_SUCCESS;
    idx: number;
    quantity: number;
}

interface IUpdateQtyItemFail {
    type: typeof UPDATE_QTY_ITEM_FAIL;
}

export const setListItems = (listItems: Array<IListItem>): ISetListItems => {
    return {
        type: SET_LIST_ITEMS,
        listItems,
    };
};


export const setCategories = (categories: Array<ICategoryObj>): ISetCategories =>  {
    return {
        type: SET_CATEGORIES,
        categories,
    }
}

export const setCategoryObj = (categoryObj: ICategoryObj): ISetCategoryObj =>  {
    return {
        type: SET_CATEGORY_OBJ,
        categoryObj,
    }
}

export const setShortageItems = (shortageItems: Array<IShortageItem>): ISetShortageItems => {
    return {
        type: SET_SHORTAGE_ITEMS,
        shortageItems,
    }
}

export const setExpiryItems = (expiryItems: Array<IExpiryItem>): ISetExpiryItems => {
    return {
        type: SET_EXPIRY_ITEMS,
        expiryItems,
    }
}


export const setIsProcessing = (): ISetIsProcessing => {
    return {
        type: SET_IS_PROCESSING,
    };
};

export const setGroupId = (group_id: number): ISetGroupId => {
    return {
        type: SET_GROUP_ID,
        group_id,
    };
};

export const deleteItemSuccess = (idx: number): IDeleteItemSuccess => {
    return {
        type: DELETE_ITEM_SUCCESS,
        idx,
    }
}

export const deleteItemFail = (): IDeleteItemFail => {
    return {
        type: DELETE_ITEM_FAIL,
    }
}

export const updateQtyItemSuccess = (idx: number, quantity: number): IUpdateQtyItemSuccess => {
    return {
        type: UPDATE_QTY_ITEM_SUCCESS,
        idx,
        quantity,
    }
}

export const updateQtyItemFail = (): IUpdateQtyItemFail => {
    return {
        type: UPDATE_QTY_ITEM_FAIL,
    }
}

export type IItemsActions = ISetListItems
                            | ISetCategories
                            | ISetCategoryObj
                            | ISetGroupId
                            | ISetShortageItems
                            | ISetExpiryItems
                            | ISetIsProcessing 
                            | IDeleteItemSuccess 
                            | IDeleteItemFail
                            | IUpdateQtyItemSuccess
                            | IUpdateQtyItemFail;

