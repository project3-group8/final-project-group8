import { DELETE_ITEM_FAIL, DELETE_ITEM_SUCCESS, IItemsActions, SET_CATEGORIES, SET_CATEGORY_OBJ, SET_EXPIRY_ITEMS, SET_GROUP_ID, SET_IS_PROCESSING, SET_LIST_ITEMS, SET_SHORTAGE_ITEMS, UPDATE_QTY_ITEM_FAIL, UPDATE_QTY_ITEM_SUCCESS } from "./actions";
import { IItemsState, initItemsState } from "./state";

export const itemsReducers = (state: IItemsState = initItemsState, action: IItemsActions): IItemsState => {
    switch (action.type) {
        case SET_IS_PROCESSING:
            return {
                ...state,
                isProcessing: true,
            };

        case DELETE_ITEM_SUCCESS:
            const listItems = state.listItems.slice();
            listItems.splice(action.idx, 1);
            return {
                ...state,
                isProcessing: false,
                listItems,
            };
        case DELETE_ITEM_FAIL:
            return {
                ...state,
                isProcessing: false,
            };

        case UPDATE_QTY_ITEM_SUCCESS:
            const listItems2 = state.listItems.slice();
            // console.log("action.idx", action.idx);
            listItems2[action.idx].quantity = action.quantity
            return {
                ...state,
                isProcessing: false,
                listItems: listItems2
            };
        case UPDATE_QTY_ITEM_FAIL:
            return {
                ...state,
                isProcessing: false,
            };

        case SET_LIST_ITEMS:
            return {
                ...state,
                listItems: action.listItems,
            }

        case SET_CATEGORIES:
            return {
                ...state,
                categories: action.categories,
            }
        case SET_CATEGORY_OBJ:
            return {
                ...state,
                categoryObj: action.categoryObj,
            }

        case SET_GROUP_ID:
            return {
                ...state,
                group_id: action.group_id,
            }
        case SET_SHORTAGE_ITEMS:
            return {
                ...state,
                shortageItems: action.shortageItems,
            }
        case SET_EXPIRY_ITEMS:
            return {
                ...state,
                expiryItems: action.expiryItems,
            }

        default:
            return state;
    }
};

