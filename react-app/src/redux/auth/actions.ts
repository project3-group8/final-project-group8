export const LOGIN_PROCESSING = '@@auth/LOGIN_PROCESSING';
export const LOGIN_SUCCESS = '@@auth/LOGIN_SUCCESS';
export const SET_USERNAME = '@@auth/SET_USERNAME';
export const LOGIN_FAIL = '@@auth/LOGIN_FAIL';
export const LOGOUT_SUCCESS = '@@auth/LOGOUT_SUCCESS';

interface ILoginProcessing {
    type: typeof LOGIN_PROCESSING;
}

interface ILoginSuccess {
    type: typeof LOGIN_SUCCESS;
}

interface ISetUsername {
    type: typeof SET_USERNAME;
    currentUser: string;
}

interface ILoginFail {
    type: typeof LOGIN_FAIL;
    message: string;
}

interface ILogoutSuccess {
    type: typeof LOGOUT_SUCCESS;
}

// action creator
export const loginProcessing = (): ILoginProcessing => {
    return {
        type: LOGIN_PROCESSING,
    };
};

export const loginSuccess = (): ILoginSuccess => {
    return {
        type: LOGIN_SUCCESS,
    };
};

export const setUsername = (currentUser: string): ISetUsername => {
    return {
        type: SET_USERNAME,
        currentUser,
    };
};

export const loginFail = (message: string): ILoginFail => {
    return {
        type: LOGIN_FAIL,
        message,
    };
};

export const logoutSuccess = (): ILogoutSuccess => {
    return {
        type: LOGOUT_SUCCESS,
    };
};

export type IAuthActions = ILoginProcessing | ILoginSuccess | ISetUsername | ILoginFail | ILogoutSuccess;
