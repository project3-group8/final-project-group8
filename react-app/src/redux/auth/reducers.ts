import { IAuthState, initAuthState } from './state';
import { IAuthActions } from './actions';
import { LOGIN_PROCESSING, LOGIN_SUCCESS, SET_USERNAME, LOGIN_FAIL, LOGOUT_SUCCESS } from './actions';
import produce from 'immer';

export const authReducers = produce(
    (
        state: IAuthState,
        action: IAuthActions
    ) => {
        switch (action.type) {
            case LOGIN_PROCESSING:
                state.isAuthenticated = false;
                state.errMessage = '';
                state.isProcessing = true;
                return;
            case LOGIN_SUCCESS:
                state.isProcessing = false;
                state.isAuthenticated = true;
                return;
            case SET_USERNAME:
                state.currentUser = action.currentUser;
                return;
            case LOGIN_FAIL:
                state.isProcessing = false;
                state.isAuthenticated = false;
                state.errMessage = action.message;
                return;
            case LOGOUT_SUCCESS:
                state.isAuthenticated = false;
                return;
            default:
                return state;
        }
    }, initAuthState);
