import { Dispatch } from 'redux';
import { IAuthActions, loginProcessing, loginSuccess, loginFail, logoutSuccess, setUsername } from './actions';
import { push, CallHistoryMethodAction } from 'connected-react-router';
import { IRootState } from '../store';
import { IItemsActions, setGroupId } from '../items/actions';

const { REACT_APP_API_SERVER } = process.env;

export const loginThunk = (username: string, password: string) => {
    return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>) => {
        // try {} catch (err) {}
        // console.log('loginThunk()');
        // console.log(REACT_APP_API_SERVER);

        dispatch(loginProcessing());

        const res = await fetch(`${REACT_APP_API_SERVER}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ username, password }),
        });
        const data = await res.json();
        // console.log(data, 'login here will show')
        if (res.status === 200) {
            localStorage.setItem('token', data.token);
            dispatch(loginSuccess());
            dispatch(push('/'));
            dispatch(setUsername(username));
        } else {
            // console.log("loginThunk", data.message);
            dispatch(loginFail(data.message));
        }
    };
};

export const restoreLoginThunk = () => {
    return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>, getState: () => IRootState) => {
        // console.log('restoreLoginThunk()');
        // try {} catch (err) {}
        const token = localStorage.getItem('token');
        if (token) {
            const res = await fetch(`${REACT_APP_API_SERVER}/users/info`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            if (res.status === 200) {
                dispatch(loginSuccess());
                dispatch(push(getState().router.location.pathname));
            } else {
                dispatch(loginFail(''));
            }
        } else {
            dispatch(loginFail(''));
        }
    };
};

export const RegisterThunk = (username: string, password: string) => {
    return async (dispatch: Dispatch) => {
        // try {} catch() {}
        const res = await fetch(`${REACT_APP_API_SERVER}/users`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ username, password }),
        });
        const data = await res.json();
        // console.log(data);
        if (res.status === 200) {
            localStorage.setItem('token', data.token);
            // loginThunk(username, password);
        } else {
            dispatch(loginFail(data.message));
        }
    };
};

export function logoutThunk() {
    return async (dispatch: Dispatch<IAuthActions | IItemsActions | CallHistoryMethodAction>) => {
        dispatch(logoutSuccess());
        dispatch(setGroupId(0));
        dispatch(setUsername(''));
        localStorage.removeItem('group_id');
        localStorage.removeItem('group_name');
        localStorage.removeItem('currentUser');
        localStorage.removeItem('token');
        dispatch(push('/'));
    };
}

export function getUsernameByTokenThunk() {
    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/users/info`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            // console.log("getUserAndGroupInfoThunk data:", data);
            localStorage.setItem('currentUser', data.user.username);
            dispatch(setUsername(data.user.username));
            // dispatch(setListItems(data.listItems));
        } else {
            console.log("getUserAndGroupInfoThunk failed")
        }
    }
}