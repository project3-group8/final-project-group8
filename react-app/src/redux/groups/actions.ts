import { IGroup, IMemberAccess, IParticipant, ISearchUsers } from './state';

// export const LOAD_GROUP_MEMBERS = "@@groups/LOAD_GROUP_MEMBERS";
export const LOAD_GROUPS_LIST = "@@groups/LOAD_GROUPS_LIST";
export const LOAD_GROUP_INFO = "@@groups/LOAD_GROUP_INFO";

export const USERS_SEARCH = "@@groups/USERS_SEARCH";
export const ADD_MEMBERS_SUCCESS = "@@groups/ADD_MEMBERS_SUCCESS";
export const ADD_MEMBERS_FAIL = "@@groups/ADD_MEMBERS_FAIL";

export const REMOVE_MEMBER_SUCCESS = "@@groups/REMOVE_MEMBER_SUCCESS";
export const REMOVE_MEMBER_FAIL = "@@groups/REMOVE_MEMBER_FAIL";

export const SET_IS_PROCESSING = '@@groups/SET_IS_PROCESSING';

export const CREATE_GROUP_SUCCESS = '@@groups/CREATE_GROUP_SUCCESS';
export const CREATE_GROUP_FAIL = '@@groups/CREATE_GROUP_FAIL';

export const LOAD_MEMBER_ACCESS = "@@groups/LOAD_MEMBER_ACCESS";
export const UPDATE_ACCESS_SUCCESS = '@@groups/UPDATE_ACCESS_SUCCESS';
export const UPDATE_ACCESS_FAIL = '@@groups/UPDATE_ACCESS_FAIL';

// interface ILoadGroupMembers {
//     type: typeof LOAD_GROUP_MEMBERS;
//     members: Array<IParticipant>;
// }
interface ILoadGroupsList {
    type: typeof LOAD_GROUPS_LIST;
    profile: Array<IGroup>;
}
interface ILoadGroupInfo {
    type: typeof LOAD_GROUP_INFO;
    info: IGroup;
}
interface ILoadUsersSearch {
    type: typeof USERS_SEARCH;
    users: Array<ISearchUsers>;
}

interface IAddMembersSuccess {
    type: typeof ADD_MEMBERS_SUCCESS;
    members: Array<IParticipant>;
}

interface IAddMembersFail {
    type: typeof ADD_MEMBERS_FAIL;
}

interface IRemoveMemberSuccess {
    type: typeof REMOVE_MEMBER_SUCCESS;
    member: IParticipant;
}

interface IRemoveMemberFail {
    type: typeof REMOVE_MEMBER_FAIL;
}
interface ISetIsProcessing {
    type: typeof SET_IS_PROCESSING;
}

interface ICreateGroupSuccess {
    type: typeof CREATE_GROUP_SUCCESS;
    group: IGroup;
}

interface ICreateGroupFail {
    type: typeof CREATE_GROUP_FAIL;
}

interface ILoadMemberAccess {
    type: typeof LOAD_MEMBER_ACCESS;
    access: IMemberAccess
}

interface IUpdateAccessSuccess {
    type: typeof UPDATE_ACCESS_SUCCESS;
    access: IMemberAccess
}

interface IUpdateAccessFail {
    type: typeof UPDATE_ACCESS_FAIL;
}

// export const loadGroupMembers = (members: Array<IParticipant>): ILoadGroupMembers => {
//     // console.log(members);
//     return {
//         type: LOAD_GROUP_MEMBERS,
//         members,
//     };
// };
export const loadGroupsList = (profile: Array<IGroup>): ILoadGroupsList => {
    // console.log(groupsList);
    return {
        type: LOAD_GROUPS_LIST,
        profile,
    };
};
export const loadGroupInfo = (info: IGroup): ILoadGroupInfo => {
    // console.log(info);
    return {
        type: LOAD_GROUP_INFO,
        info,
    };
};
export const loadUsersSearch = (users: Array<ISearchUsers>): ILoadUsersSearch => {
    // console.log(users);
    return {
        type: USERS_SEARCH,
        users,
    };
};

export const addMembersSuccess = (members: Array<IParticipant>): IAddMembersSuccess => {
    // console.log(members);
    return {
        type: ADD_MEMBERS_SUCCESS,
        members,
    };
};
export const addMembersFail = (): IAddMembersFail => {
    return {
        type: ADD_MEMBERS_FAIL,
    };
};

export const removeMemberSuccess = (member: IParticipant): IRemoveMemberSuccess => {
    // console.log(member);
    return {
        type: REMOVE_MEMBER_SUCCESS,
        member,
    };
};
export const removeMemberFail = (): IRemoveMemberFail => {
    return {
        type: REMOVE_MEMBER_FAIL,
    };
};
export const setIsProcessing = (): ISetIsProcessing => {
    return {
        type: SET_IS_PROCESSING,
    };
};

export const createGroupSuccess = (group: IGroup): ICreateGroupSuccess => {
    return {
        type: CREATE_GROUP_SUCCESS,
        group,
    };
};

export const createGroupFail = (): ICreateGroupFail => {
    return {
        type: CREATE_GROUP_FAIL,
    };
};
export const loadMemberAccess = (access: IMemberAccess): ILoadMemberAccess => {
    // console.log(`${access} in loadMemberAccess`);
    return {
        type: LOAD_MEMBER_ACCESS,
        access,
    };
};


export const updateAccessSuccess = (access: IMemberAccess): IUpdateAccessSuccess => {
    // console.log(`${access} in updateAccessSuccess`);
    return {
        type: UPDATE_ACCESS_SUCCESS,
        access,
    };
};

export const updateAccessFail = (): IUpdateAccessFail => {
    return {
        type: UPDATE_ACCESS_FAIL,
    };
};


export type IGroupsActions =
    | ILoadGroupsList
    | ILoadGroupInfo
    // | ILoadGroupMembers
    | ILoadUsersSearch
    | IAddMembersSuccess
    | IAddMembersFail
    | IRemoveMemberSuccess
    | IRemoveMemberFail
    | ISetIsProcessing
    | ICreateGroupSuccess
    | ICreateGroupFail
    | ILoadMemberAccess
    | IUpdateAccessSuccess
    | IUpdateAccessFail;

