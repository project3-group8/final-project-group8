import { IRootState } from '../store';
import { Dispatch } from 'redux';
import { loginFail } from '../auth/actions';
import {
    // loadGroupMembers,
    loadGroupInfo,
    loadGroupsList,
    loadUsersSearch,
    addMembersSuccess,
    addMembersFail,
    removeMemberSuccess,
    removeMemberFail,
    setIsProcessing,
    createGroupSuccess,
    createGroupFail,
    updateAccessFail,
    updateAccessSuccess,
    loadMemberAccess
} from './actions';
import { IParticipant } from './state';
// import { push, CallHistoryMethodAction } from 'connected-react-router';


const { REACT_APP_API_SERVER } = process.env;


export const createGroupThunk = (group_name: string) => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        dispatch(setIsProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/users/newGp`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ group_name }),
        });
        // localStorage.removeItem('token');
        // console.log(`after fetch ${group_name} work `)
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            // console.log(data, `in create Group thunk`);
            const newGroup = {
                group_id: data.group_id,
                group_name,
                user_id: data.user_id,
                owner: data.owner,
                access_right: 'owner',
                invitation_code: data.token,
                members: Array<IParticipant>(
                    {
                        user_id: data.user_id,
                        username: data.owner,
                        access_right: 'owner'
                    }
                )//nothing 
            };
            // console.log(newGroup);
            dispatch(createGroupSuccess(newGroup));
            // console.log(`dispatch new GP -${newGroup} success`)
            await getGroupsListThunk();
        } else {
            dispatch(createGroupFail());
        }
    };
};
export const removeMemberThunk = (groupID: number, removeMemberID: number) => {

    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        dispatch(setIsProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/users/remove/${groupID}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ groupID, removeMemberID }),
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            // console.log(data);
            // const removeMember = {
            //     user_id: data.id,
            //     username: data.username,
            //     access_right: data.access_right
            // };
            // console.log(removeMember);
            dispatch(removeMemberSuccess(data));
            await getGpDetailAndMembersThunk(groupID);
        } else {
            // console.log("remove fail")
            dispatch(removeMemberFail());
        }
    };
};

export const addMembersThunk = (groupID: number, addMembersArr: []) => {
    // console.log(addMembersArr);
    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        dispatch(setIsProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/users/add/${groupID}`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ groupID, addMembersArr }),
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            await res.json();
            const addMembers = addMembersArr.map((addMember: any) => (
                {
                    group_id: groupID,
                    user_id: addMember.user_id,
                    username: addMember.name,
                    access_right: 'reader',
                }))
            // console.log(addMembers);
            dispatch(addMembersSuccess(addMembers));
            await getGpDetailAndMembersThunk(groupID);
        } else {
            // console.log("add fail")
            dispatch(addMembersFail());
        }
    };
};
export const getUSearchThunk = (groupID: number) => {
    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        dispatch(setIsProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/users/uSearch/${groupID}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            getGpDetailAndMembersThunk(groupID);
            dispatch(loadUsersSearch(data.users));
        }
    };
};


// export const getGpInfoThunk = (group_id: number) => {
//     return async (dispatch: Dispatch) => {
//         const token = localStorage.getItem('token');
//         if (!token) {
//             dispatch(loginFail(''));
//         }
//         const res = await fetch(`${REACT_APP_API_SERVER}/users/GpInfo/${group_id}`, {
//             headers: {
//                 Authorization: `Bearer ${token}`,
//             },
//         });
//         if (res.status === 401) {
//             dispatch(loginFail(''));
//         }
//         if (res.status === 200) {
//             const data = await res.json();
//             dispatch(loadGroupsList(data.info));
//         }
//     };
// };

// export const getGpMembersThunk = (group_id: number) => {
//     return async (dispatch: Dispatch) => {
//         const token = localStorage.getItem('token');
//         if (!token) {
//             dispatch(loginFail(''));
//         }
//         const res = await fetch(`${REACT_APP_API_SERVER}/users/GpMembers/${group_id}`, {
//             headers: {
//                 Authorization: `Bearer ${token}`,
//             },
//         });
//         if (res.status === 401) {
//             dispatch(loginFail(''));
//         }
//         if (res.status === 200) {
//             const data = await res.json();
//             dispatch(loadGroupMembers(data.members));
//         }
//     };
// };
export const getGpDetailAndMembersThunk = (groupID: number) => {
    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        // console.log(`is thunk work --- getGpDetailAndMembersThunk b4 fetch`)
        dispatch(setIsProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/users/GpInfo/${groupID}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            // console.log(data.info);
            dispatch(loadGroupInfo(data.info[0]));
        }
    };
};

export const getGroupsListThunk = () => {
    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        dispatch(setIsProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/users/group`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            // console.log(data);
            dispatch(loadGroupsList(data.groups));
        }
    };
};
export const getAccessThunk = (groupID: number) => {
    // if (groupID = 0) {
    //     console.log('stop getAccess until receive Access')
    //     return
    // }
    // console.log(`show me is this work : getAccessThunk`)

    return async (dispatch: Dispatch) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        dispatch(setIsProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/users/${groupID}/access`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            dispatch(loadMemberAccess(data.access[0]));
        }
    };
};

export const updateAccessThunk = (
    groupID: number,
    UpdateMemberID: number,
    MemberAccess: string
) => {
    return async (dispatch: Dispatch) => {
        // console.log("In updateAccessThunk");
        // console.log(groupID);
        // console.log(UpdateMemberID);
        // console.log(MemberAccess);
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        dispatch(setIsProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/users/${groupID}/updateAccess`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                group_id: groupID,
                user_id: UpdateMemberID,
                access_right: MemberAccess
            }),
        });
        // console.log(groupID, `after fetch`);
        // console.log(UpdateMemberID, `after fetch`);
        // console.log(MemberAccess, `after fetch`);
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const data = await res.json();
            // console.log(data);
            dispatch(updateAccessSuccess(data.updateMember));
            await getGpDetailAndMembersThunk(groupID);
        } else {
            // console.log("update fail")
            dispatch(updateAccessFail());
        }
    };
}
