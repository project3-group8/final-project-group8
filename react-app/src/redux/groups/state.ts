// step 1: interface
export interface ISearchUsers {
    user_id: number;
    username: string;
}
export interface IParticipant {
    user_id: number;
    username: string;
    access_right: string;
}
export interface IMemberAccess {
    group_id: number;
    user_id: number;
    access_right: string | undefined;
}
export interface IGroup {
    group_id: number;
    group_name: string;
    owner: string;
    invitation_code: string;
    members: Array<IParticipant>;
    myAccess?: string;
}
// export interface IGroupMapping {
//     [id: number]: IGroup;
// }
export interface IGroupsState {
    isProcessing: boolean;
    profile: Array<IGroup>;
    users: Array<ISearchUsers>;
    info: IGroup;
    // members: Array<IParticipant>;
    access: IMemberAccess;
}

// step 2: initState
export const initGroupsState: IGroupsState = {
    isProcessing: false,
    profile: [] as Array<IGroup>,
    users: [] as Array<ISearchUsers>,
    info: {} as IGroup,
    // members: [] as Array<IParticipant>,//new Array<IParticipant>()
    access: {} as IMemberAccess,
};
