import { IGroupsState, initGroupsState } from "./state";
import {
    IGroupsActions,
    LOAD_GROUPS_LIST,
    LOAD_GROUP_INFO,
    // LOAD_GROUP_MEMBERS,
    USERS_SEARCH,
    ADD_MEMBERS_SUCCESS,
    ADD_MEMBERS_FAIL,
    REMOVE_MEMBER_SUCCESS,
    REMOVE_MEMBER_FAIL,
    SET_IS_PROCESSING,
    CREATE_GROUP_SUCCESS,
    CREATE_GROUP_FAIL,
    LOAD_MEMBER_ACCESS,
    UPDATE_ACCESS_SUCCESS,
    UPDATE_ACCESS_FAIL
} from "./actions";
import produce from 'immer';

export const groupsReducers = produce(
    (
        state: IGroupsState,
        action: IGroupsActions
    ) => {
        switch (action.type) {
            // case LOAD_GROUP_INFO:
            //     state.profile[0] = action.info;
            //     return;

            // case LOAD_GROUP_MEMBERS:
            //     state.members = action.members;
            //     return;
            case LOAD_GROUPS_LIST:
                state.profile = action.profile;
                return;
            case LOAD_GROUP_INFO:
                state.info = action.info;
                return;
            case USERS_SEARCH:
                state.users = action.users;
                return;
            case ADD_MEMBERS_SUCCESS:
                const addMembers: any = action.members.map((addMember: any) => (
                    {
                        group_id: addMember.group_id,
                        user_id: addMember.user_id,
                        username: addMember.name,
                        access_right: addMember.access_right,
                    }))
                const theGpNeedAddMembers = state.profile.slice();
                theGpNeedAddMembers.push(addMembers);
                state.isProcessing = false;
                return;
            case ADD_MEMBERS_FAIL:
                state.isProcessing = false;
                return;
            case REMOVE_MEMBER_SUCCESS:
                const theGpNeedRemoveMember = state.profile.slice();
                theGpNeedRemoveMember.splice(action.member.user_id, 1);
                state.isProcessing = false;
                return;
            case REMOVE_MEMBER_FAIL:
                state.isProcessing = false;
                return;
            case SET_IS_PROCESSING:
                state.isProcessing = true;
                return;
            case CREATE_GROUP_SUCCESS:
                state.profile.unshift(action.group);
                state.isProcessing = false;
                return;
            case CREATE_GROUP_FAIL:
                state.isProcessing = false;
                return;
            case LOAD_MEMBER_ACCESS:
                state.access = action.access;
                return;
            case UPDATE_ACCESS_SUCCESS:
                state.access = action.access;
                state.isProcessing = false;
                return;
            case UPDATE_ACCESS_FAIL:
                state.isProcessing = false;
                return;
            default:
                return state;
        }
    }, initGroupsState);
