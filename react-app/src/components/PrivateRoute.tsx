import React from 'react';
import { RouteProps, Redirect, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import Navbar from './Navbar';

const PrivateRoute: React.FC<RouteProps> = ({ component, ...rest }) => {
    const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated);
    const Component = component as any;
    if (Component === null) {
        return null;
    }

    let render: (props: any) => JSX.Element;
    if (isAuthenticated) {
        render = (props: any) => (
            <>
                <Navbar />
                <Component {...props} />
            </>
        )

    } else {
        render = (props: any) => (
            <Redirect
                to={{
                    pathname: '/login',
                    state: { from: props.location },
                }}
            />
        );
    }
    return <Route {...rest} render={render} />;
};

export default PrivateRoute;
