import React from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { IListItemProps } from "./ItemsListItem";
import ItemUpdateQtyForm from "./ItemUpdateQtyForm";
import '../css/ItemUpdateQtyModal.css';
import { Button } from '@material-ui/core';
interface IItemUpdateQtyModalProps {
    closeModal: () => void;
    props: IListItemProps;
}

const ItemUpdateQtyModal: React.FC<IItemUpdateQtyModalProps> = ({ closeModal, props }) => {
    // console.log("Props", props);
    return (
        <div>
            <Modal isOpen={true} >
                <ModalHeader >Update Quantity</ModalHeader>
                <ModalBody>
                    <ItemUpdateQtyForm closeModal={closeModal} props={props} />
                </ModalBody>
                <ModalFooter>
                    <Button
                        className="UpdateQtyReturn"
                        // color="primary"
                        // variant="outlined"
                        onClick={closeModal}
                    >
                        RETURN
                        </Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default ItemUpdateQtyModal;