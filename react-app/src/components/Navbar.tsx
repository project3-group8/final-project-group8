import React from 'react';
import { NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { logoutThunk } from '../redux/auth/thunks';
import '../css/Navbar.css';
// import removebg from '../removebg.png';
import GroupMenuButton from './GroupMenuButton';
import {
    Button,
    ListItemIcon,
    ListItemText,
    withStyles,
    MenuProps,
    Menu,
    MenuItem,
    Badge,
    Avatar
} from '@material-ui/core';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import InsertChartOutlinedRoundedIcon from '@material-ui/icons/InsertChartOutlinedRounded';
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle';
// import FindInPageIcon from '@material-ui/icons/FindInPage';
import AssignmentIndRoundedIcon from '@material-ui/icons/AssignmentIndRounded';
import { IRootState } from '../redux/store';
// import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import PersonIcon from '@material-ui/icons/Person';


const StyledMenu = withStyles({
    // paper: {
    //     border: '1px solid #d3d4d5',
    // },
})((props: MenuProps) => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles((theme) => ({
    // root: {
    //     '&:focus': {
    //         backgroundColor: theme.palette.primary.main,
    //         '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
    //             color: theme.palette.common.white,
    //         },
    //     },
    // },
}))(MenuItem);

const Navbar: React.FC = () => {
    const dispatch = useDispatch();
    const currentUser = useSelector((state: IRootState) => state.auth.currentUser);
    // localStorage.setItem('currentUser', currentUser);// avoid lost of username in page change/refresh
    // const currentUser = localStorage.getItem('currentUser');
    const shortageItems = useSelector((state: IRootState) => state.items.shortageItems);
    const expiryItems = useSelector((state: IRootState) => state.items.expiryItems);

    const [anchorEl2, setAnchorEl2] = React.useState<null | HTMLElement>(null);

    const handle22Click = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl2(event.currentTarget);
        console.log(event.currentTarget.id);
    };

    const handleClose2 = () => {
        setAnchorEl2(null);
    };

    // console.log("Shortage Items:", shortageItems);
    // console.log("Shortage Items length:", shortageItems.length);

    return (
        <div className="Navbar">
            {
                < NavLink to="/notify" exact={true} className="Navbar_link">
                    <Badge badgeContent={shortageItems.length + expiryItems.length} max={10} color="secondary">
                        <NotificationsNoneIcon />
                    </Badge>
                </NavLink>
            }

            {
                <NavLink to="/chart" exact={true} className="Navbar_link">
                    <InsertChartOutlinedRoundedIcon />
                </NavLink>
            }

            {
                <NavLink to="" className="Navbar_link_currentPlace">
                    {/* <FindInPageIcon /> */}
                    {/* <img className="removebg"
                        src={removebg}>
                    </img> */}
                    <PersonPinCircleIcon fontSize="small" />
                    {localStorage.getItem('group_name')}
                </NavLink>
            }

            {
                <NavLink to='/' exact={true} className="Navbar_link">
                    <GroupMenuButton />
                </NavLink>
            }

            {
                <NavLink to="/" exact={true} className="Navbar_link">
                    <Button
                        aria-controls="profile-menu"
                        aria-haspopup="true"
                        onClick={handle22Click}
                    >
                        {/* <AssignmentIndRoundedIcon /> */}
                        <Avatar alt={currentUser}
                            // src='/broken-image.jpg'
                            className="Navbar_Avatar">
                        </Avatar>
                    </Button>

                    <StyledMenu
                        id="profile-menu"
                        anchorEl={anchorEl2}
                        keepMounted
                        open={Boolean(anchorEl2)}
                        onClose={handleClose2}
                    >
                        {/* <StyledMenuItem>
                            <ListItemIcon>
                                <FindInPageIcon fontSize="small" />
                            </ListItemIcon>
                            <ListItemText primary="Preference" />
                        </StyledMenuItem> */}


                        <StyledMenuItem>
                            <ListItemIcon>
                                <PersonIcon fontSize="small" />
                            </ListItemIcon>
                            <ListItemText primary={localStorage.getItem('currentUser')} />
                        </StyledMenuItem>

                        {/* <StyledMenuItem>
                            <ListItemIcon>
                                <PersonPinCircleIcon fontSize="small" />
                            </ListItemIcon>
                            <ListItemText primary={localStorage.getItem('group_name')} />
                        </StyledMenuItem> */}

                        <StyledMenuItem onClick={() => dispatch(logoutThunk())}>
                            <ListItemIcon>
                                <AssignmentIndRoundedIcon fontSize="small" />
                            </ListItemIcon>
                            <ListItemText primary="Logout" />
                        </StyledMenuItem>
                    </StyledMenu>

                </NavLink>
            }

        </div >
    )
}

export default Navbar
