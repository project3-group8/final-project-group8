import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { getGroupsListThunk, createGroupThunk } from '../redux/groups/thunks';
import GroupMenuItem from './GroupMenuItem';
import {
    Button,
    ListItemIcon,
    ListItemText,
    withStyles,
    MenuProps,
    Menu,
    MenuItem,
    TextField,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText
} from '@material-ui/core';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import '../css/GroupMenuButton.css';


const StyledMenu = withStyles({
    // paper: {
    //     border: '1px solid #d3d4d5',
    // },
})((props: MenuProps) => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles((theme) => ({
    // root: {
    //     '&:focus': {
    //         backgroundColor: theme.palette.primary.main,
    //         '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
    //             color: theme.palette.common.white,
    //         },
    //     },
    // },
}))(MenuItem);

const GroupMenuButton: React.FC = () => {
    const dispatch = useDispatch();
    const profile = useSelector((state: IRootState) => state.groups.profile);
    // const token = localStorage.getItem('token');
    // const filterUser = profile.filter(profile => profile.members[0].user_id === 2);//req.user?.id
    // console.log(filterUser);
    // const filterUser = profile.filter(profile => profile.members[0].user_id === 2);
    // console.log(filterUser);

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
        // console.log(event.currentTarget.id);
        event.preventDefault();
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const [open, setOpen] = React.useState(false);

    const handleDialogClickOpen = () => {
        // console.log(`can open`);
        setOpen(true);
    };

    const handleDialogClose = () => {
        // console.log(`can close`);
        setOpen(false);
    };

    useEffect(() => {
        dispatch(getGroupsListThunk());
    }, [dispatch]);

    const [newGroupName, setNewGroupName] = useState('');
    const newGPHandler = async () => {

        // console.log(newGroupName);
        // console.log(setNewGroupName);
        // if (!setIsProcessing) {
        // console.log('start create');

        if (newGroupName !== '') {
            await dispatch(createGroupThunk(newGroupName));
            //handleDialogClose();
            setOpen(false);
            await dispatch(getGroupsListThunk());
        // } else {
        //     console.log('Empty groupname is not allowed');
        }

    };

    // console.log('test', open)

    return (
        <>
            <Button
                aria-controls="group-menu"
                aria-haspopup="true"
                onClick={handleClick}
                id="group-menu"
            >
                <PeopleAltIcon />
            </Button>

            <StyledMenu
                id="group-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            // onClick={() => dispatch(getGroupsListThunk())}
            >

                {profile && profile.length > 0 && (
                    <div>
                        {
                            profile.map((item, idx) => (
                                <GroupMenuItem
                                    key={`items_${idx}`}
                                    group_id={item.group_id}
                                    group_name={item.group_name}
                                    // //owner={item.owner}
                                    // //invitation_code={item.invitation_code}
                                    members={item.members}
                                    handleClose={handleClose}
                                />
                            ))
                        }
                    </div>
                )}

                <StyledMenuItem onClick={handleDialogClickOpen}>
                    <ListItemIcon>
                        <GroupAddIcon fontSize="large" />
                    </ListItemIcon>
                    <ListItemText primary="Create Group" id="group-menu-CreateGroup" />


                </StyledMenuItem>
            </StyledMenu>

            <Dialog open={open} onClose={handleDialogClose} aria-labelledby="set_new_group_name">
                <DialogContent>
                    <DialogContentText>
                        Simply share the inventories data with your family, friends, organizations, classes, teams together.
                        Let's create your new group here!
                            </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        className="NewGroupInput"
                        id="name"
                        label="Group Name"
                        type="name"
                        value={newGroupName}
                        onChange={(e) => setNewGroupName(e.target.value)}
                        fullWidth
                    />
                </DialogContent>

                <DialogActions>
                    <Button
                        className="CreateGroupReturn"
                        onClick={(e) => {
                            // console.log(`set open is work?!`)
                            setOpen(false)
                        }}
                    >
                        RETURN
                    </Button>

                    <Button onClick={newGPHandler}
                        className="GroupCreate"
                    >
                        CREATE
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    )
}

export default GroupMenuButton;
