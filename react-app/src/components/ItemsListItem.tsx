import React, { useState } from 'react';
import { ListItem, ListItemText, IconButton } from '@material-ui/core';
// import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
// import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
// import LocalOfferIcon from '@material-ui/icons/LocalOffer';
// import LoyaltyIcon from '@material-ui/icons/Loyalty';
import LabelImportantIcon from '@material-ui/icons/LabelImportant';
import EventIcon from '@material-ui/icons/Event';
import moment from 'moment';
// import { useDispatch } from 'react-redux';
// import { deleteItemThunk } from '../redux/items/thunks';
import ItemUpdateQtyModal from './ItemUpdateQtyModal';
// import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import '../css/ItemsListItem.css';
import ItemImageModal from './ItemImageModal';
import ItemDeleteItemModal from './ItemDeleteItemModal';
// import { IRootState } from '../redux/store';
// import { getAccessThunk } from '../redux/groups/thunks';
import ExposureIcon from '@material-ui/icons/Exposure';
// import { fontWeight } from '@material-ui/system';


const { REACT_APP_API_SERVER_IMAGE } = process.env;

export interface IListItemProps {
    index_value: number;
    item_id: number;
    product: string;
    category: string;
    user_id: number;
    group_id: number;
    expiry_date: Date;
    quantity: number;
    image: string;
    inventory_id: number;
    access: string | undefined;
}

const ItemsListItem: React.FC<IListItemProps> = (props) => {

    // const dispatch = useDispatch();
    // const deleteItem = () => {
    //     dispatch(deleteItemThunk(props));
    // }

    const [showDeleteModal, setShowDeleteModal] = useState(false);

    // console.log("showDeleteModal", showDeleteModal);

    const [showUpdateModal, setShowUpdateModal] = useState(false);
    // console.log("showUpdateModal", showUpdateModal);

    const [showImageModal, setShowImageModal] = useState(false);
    // console.log("props in ItemsListItem", props)

    const displayURL = props.image;
    // const validURL = (displayURL: any) => {
    //     var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
    //         '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    //         '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    //         '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    //         '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    //         '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    //     if (pattern.test(displayURL)) {
    //         return displayURL;
    //     } else {
    //         //local
    //         const filePath = `http://localhost:8080/${props.image}`
    //         // const filePath = `REACT_APP_API_SERVER${props.image}`
    //         console.log(filePath)
    //         return filePath;
    //     }
    // }

    function validURL2(displayURL: any) {
        if (displayURL.substring(0, 4) === 'http') {
            return displayURL;
        } else {
            // return `${REACT_APP_API_SERVER_IMAGE}/${props.image}`;
            return `https://project3-group8-uploads.s3-ap-southeast-1.amazonaws.com/${props.image}`
            // for local: return `http://localhost:8080/${props.image}`
            // for EC2: return `https://api.groctake.life/${props.image}
        }
    }

    //display time
    const receivedTime = props.expiry_date;
    // console.log(props.expiry_date);
    const validDisplayTimeOrWords = (receivedTime: any) => {
        //domain
        // console.log(`${moment.utc('3899-12-31T16:00:00.000+08:00').utcOffset("+08:00").toISOString()}`, '23?!');//3899-12-31T08:00:00.000Z
        // console.log(`${moment.utc('3899-12-31T16:00:00.000+08:00').utcOffset("+08:00", true).toISOString()}`, '22?!');//3899-12-31T00:00:00.000Z
        // console.log(`${(moment.utc('3899-12-31T16:00:00.000+08:00').utcOffset("+08:00", true)).toISOString()}`, '21?!');//3899-12-31T00:00:00.000Z
        // console.log(`${(moment.utc('3899-12-31T16:00:00.000+08:00').utcOffset(8, true)).toISOString()}`, '20?!');//3899-12-31T00:00:00.000Z
        // console.log(`${moment.utc('3899-12-31T16:00:00.000+08:00').toISOString()}`, '19?!');//3899-12-31T08:00:00.000Z
        // console.log(`${(moment.utc('3899-12-31T16:00:00.000+08:00').utcOffset(8, true)).toISOString()}`, '18?!');//3899-12-31T00:00:00.000Z
        // console.log(`${moment.utc('3899-12-31T16:00:00.000+08:00').utc(true).toISOString()}`, '17?!');//3899-12-31T08:00:00.000Z
        // console.log(`${(moment.utc('3899-12-31T16:00:00.000+08:00').utc(true)).toISOString()}`, '16?!');//3899-12-31T08:00:00.000Z
        // console.log(`${(moment.parseZone('3899-12-31T16:00:00.000+08:00').utc(true)).toISOString()}`, '15?!');//3899-12-31T16:00:00.000Z
        // console.log(`${(moment.parseZone('3899-12-31T16:00:00.000+08:00').utcOffset(0, true)).toISOString()}`, '14?!');//3899-12-31T16:00:00.000Z
        // console.log(`${moment.parseZone('3899-12-31T16:00:00.000+08:00').utcOffset(8, true).toISOString()}`, '13?!');//3899-12-31T08:00:00.000Z
        // // console.log(`${moment.parseZone('3899-12-31T16:00:00.000+08:00').utcOffset(8, true).format()}`, '12?!');//3899-12-31T16:00:00+08:00
        // console.log(`${moment.parseZone('3899-12-31T16:00:00.000+08:00').utc().toISOString()}`, '11?!');//3899-12-31T08:00:00.000Z
        // // console.log(`${moment.utc('3899-12-31T16:00:00.000+08:00').utcOffset(8, true).format()}`, '10?!');//3899-12-31T08:00:00+08:00
        // console.log(`${moment.parseZone('3899-12-31T16:00:00.000+08:00').utc(true).toISOString()}`, '9?!');//3899-12-31T16:00:00.000Z
        // // console.log(`${moment.utc('3899-12-31T16:00:00.000').utcOffset("+08:00", true).format()}`, '8?!');//3899-12-31T16:00:00+08:00
        // // console.log(`${moment.utc('3899-12-31T16:00:00.000').utcOffset(0, true).format()}`, '7?!');//3899-12-31T16:00:00Z without sss
        // // console.log(`${moment.utc('3899-12-31T16:00:00+08:00').utcOffset(0, true).format()}`, '6?!');//same without sss 3899-12-31T08:00:00Z
        // console.log(`${moment.utc('3899-12-31T16:00:00+08:00').toISOString()}`, '5?!');//3899-12-31T08:00:00.000Z
        // // console.log(`${moment.utc('3899-12-31T16:00:00+08:00')}`, '4?!');//Sun Dec 31 3899 08:00:00 GMT+0000 4?!
        // // console.log(`${moment.parseZone('3899-12-31T16:00:00+08:00').utcOffset(0, true).format()}`, '3?!');//3899-12-31T16:00:00Z
        // console.log(`${moment.parseZone('3899-12-31T16:00:00+08:00').utc(true).toISOString()}`, 'domain2?!');//3899-12-31T16:00:00.000Z
        // console.log(`${moment.parseZone('3899-12-31T16:00:00+08:00').utc().toISOString()}`, 'local2?!');//no change date T0000 3899-12-31T08:00:00.000Z
        // console.log(`${moment.parseZone('3899-12-31T16:00:00.000').utc(true).toISOString()}`, 'domain?!');//3899-12-31T16:00:00.000Z
        // console.log(`${moment.parseZone('3899-12-31T16:00:00.000').utc().toISOString()}`, 'local?!');//3899-12-31T16:00:00.000Z
        // if (receivedTime !== moment.parseZone('3899-12-31T16:00:00.000').utc().toISOString()) {
        if (receivedTime !== '3900-01-01T00:00:00.000Z') {
            // HKSAR local
            // if (receivedTime !== '3899-12-31T16:00:00.000Z') {
            const RelativeTime = moment(props.expiry_date).endOf('hour').fromNow();
            return RelativeTime;
        } else {
            const NoExpiredDate = 'No Expired Date';
            return NoExpiredDate;
        }
    }

    // const group_id = useSelector((state: IRootState) => state.items.group_id);
    // useEffect(() => {
    //     dispatch(getAccessThunk(group_id));
    // }, [dispatch]);

    //display edit & del
    // const [accessAvailableToShow, setAccessAvailableToShow] = useState(false);
    // const access = useSelector((state: IRootState) => state.groups.access)
    // console.log(access);
    // console.log(access.access_right);
    // useEffect(() => {
    //     // console.log(access, `is change ??? `)
    //     if (access) {
    //         const AccessAvailable = access.access_right
    //         // console.log(`${ AccessAvailable } show it now!!!!`);
    //         if (AccessAvailable !== 'reader' || undefined) {
    //             return setAccessAvailableToShow(true);
    //         } else {
    //             return setAccessAvailableToShow(false);
    //         }
    //     }
    // }, [access])

    return (
        <>
            <ListItem className='ItemsListItem'>
                {/* may use later
                <div>
            'fav null<LocalOfferIcon/> | add wishlist<LoyaltyIcon />'  
                </div>
                */}
                <div className="item-img">
                    <img src={validURL2(displayURL)} onClick={() => setShowImageModal(true)} className="imgIcon"/>
                    {/* <img src={validURL2(displayURL)} onClick={() => setShowImageModal(true)} className="imgIcon" alt="No Image"/> */}
                    {/* <img src={`http://localhost:8080/${props.image}`} onClick={() => setShowImageModal(true)} className="imgIcon" /> */}
                </div >

                <div>
                    <LabelImportantIcon />
                </div>

                <div className="item-product">
                    <ListItemText primary={props.product} />
                </div>

                <div className="item-category">
                    <ListItemText secondary={props.category} />
                </div>

                <div className="item-cal">
                    <EventIcon />
                    {validDisplayTimeOrWords(receivedTime)}
                </div>

                {
                    (props.access !== "reader" || undefined) &&
                    // accessAvailableToShow &&
                    <div className="item-edit"
                    // onChange={() => setAccessAvailableToShow(true)}
                    >
                        <IconButton onClick={() => { setShowUpdateModal(true) }} edge="end" aria-label="add">
                            {/* <AddCircleOutlineIcon /> */}
                            {/* <EditIcon /> */}
                            <ExposureIcon />
                        </IconButton>
                    </div>
                }

                <div className="item-quantity">
                    {props.quantity}
                </div>

                {
                    (props.access !== "reader" || undefined) &&
                    // accessAvailableToShow &&
                    <div className="item-del"
                    // onChange={() => setAccessAvailableToShow(true)}
                    >
                        <IconButton onClick={() => { setShowDeleteModal(true) }} edge="end" aria-label="remove">
                            {/* <RemoveCircleOutlineIcon /> */}
                            <DeleteIcon />
                        </IconButton>
                    </div>
                }


            </ListItem >

            {showDeleteModal && <ItemDeleteItemModal closeModal={() => setShowDeleteModal(false)}
                props={props} />}

            {showUpdateModal && <ItemUpdateQtyModal closeModal={() => setShowUpdateModal(false)}
                props={props} />}

            {
                showImageModal && <ItemImageModal closeModal={() => setShowImageModal(false)}
                    props={props} />
            }
        </>

    );
}

export default ItemsListItem;