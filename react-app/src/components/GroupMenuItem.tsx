import React from 'react';

import {
    ListItemText,
    withStyles,
    MenuItem,
    Badge,
    ListItemSecondaryAction,
    IconButton
} from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { IParticipant } from '../redux/groups/state';
// import { getListItemsByUserByGroupByCatThunk } from '../redux/items/thunks';
// import { IRootState } from '../redux/store';
import { setCategoryObj, setGroupId } from '../redux/items/actions';
import '../css/GroupMenuItem.css';

interface IGroupsMenuItemProps {
    group_id: number;
    group_name: string;
    // owner: string;
    // invitation_code: string;
    members: Array<IParticipant>;
    handleClose: () => void;
}

const StyledMenuItem = withStyles((theme) => ({
    // root: {
    //     '&:focus': {
    //         backgroundColor: theme.palette.primary.main,
    //         '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
    //             color: theme.palette.common.white,
    //         },
    //     },
    // },
}))(MenuItem);



const GroupsMenuItem: React.FC<IGroupsMenuItemProps> = (props) => {
    const dispatch = useDispatch();
    // console.log(props.group_id);
    //error for create Gp
    // const groupID = (props.group_id).toString();
    // const landToGpId = (`/gp/${(props.group_id).toString()}`);
    // console.log(landToGpId);

    const GroupItemsHandler = () => {
        // console.log(props.group_id);
        // console.log(`change to display items for ${props.group_id}`);
        // let category = 'All'
        // dispatch(getListItemsByUserByGroupByCatThunk(props.group_id, category))
        localStorage.setItem('group_id', props.group_id.toString());    // avoid lost of group_id in page refresh
        localStorage.setItem('group_name', props.group_name);
        dispatch(setGroupId(props.group_id));
        dispatch(setCategoryObj({ category: "All" }));
        props.handleClose();
    }

    const GroupPageHandler = () => {
        localStorage.setItem('group_id', props.group_id.toString());
        localStorage.setItem('group_name', props.group_name);
        dispatch(setGroupId(props.group_id));
        dispatch(setCategoryObj({ category: "All" }));
        props.handleClose();
    }

    return (
        <div>
            <StyledMenuItem
                key={props.group_id}
                button
                onClick={GroupItemsHandler}
            >
                <ListItemText
                    primary={props.group_name}
                    id={`'${props.group_id}'`}
                />

                <ListItemSecondaryAction>
                    <Link to={`/gp/${props.group_id}`}>
                        <IconButton
                            onClick={GroupPageHandler}
                        >
                            <Badge
                                color="default"
                                badgeContent={props.members.length}
                                max={9}
                                showZero
                            />
                        </IconButton>
                    </Link>

                </ListItemSecondaryAction>

            </StyledMenuItem>
        </div>
    )
}

export default GroupsMenuItem
