import React from 'react'
import GroupMember from './GroupMember'
import { List } from '@material-ui/core'
import { IParticipant } from '../redux/groups/state';

// import { useDispatch} from 'react-redux';
// import { IRootState } from '../redux/store';
// import { getGroupsListThunk } from '../redux/groups/thunks';
// import { getGpMembersThunk } from '../redux/groups/thunks';

interface IGroupListProps {
    group_id: number;
    members: Array<IParticipant> | null;
    access: string | undefined;
}

const GroupList: React.FC<IGroupListProps> = (props) => {
    // const dispatch = useDispatch();
    // const profile = useSelector((state: IRootState) => state.groups.profile);
    // const filterMembers = profile.find(profile => profile.group_id === props.group_id);
    // console.log(filterMembers);


    // useEffect(() => {
    // dispatch(getGroupsListThunk());
    // dispatch(getGpMembersThunk((props.group_id)));
    // dispatch(getGpDetailThunk((props.group_id)));
    // }, [dispatch]);
    return (
        <>
            {props.members && props.members.length > 0 && (
                <List className="MembersList">
                    <>
                        {props.members.map((member, idx) => (
                            <GroupMember
                                key={`members_${idx}`}
                                user_id={member.user_id}
                                username={member.username}
                                access_right={member.access_right}
                                group_id={props.group_id}
                                access={props.access}
                            // group_name={member.group_name}
                            // members={Array<IParticipant>}
                            />
                        ))}
                    </>
                </List>
            )}
        </>
    )
}

export default GroupList
