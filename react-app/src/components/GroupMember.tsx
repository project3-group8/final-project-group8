import React from 'react'
// import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { Select, InputLabel, FormControl, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { getGpDetailAndMembersThunk, getGroupsListThunk, getUSearchThunk, removeMemberThunk, updateAccessThunk } from '../redux/groups/thunks';
import { useDispatch } from 'react-redux';
import '../css/GroupMember.css';
// import { IRootState } from '../redux/store';
interface IGroupMemberProps {
    group_id: number;
    user_id: number;
    username: string;
    access_right: string;
    access: string | undefined;
}

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        fontSize: 12
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const GroupMember: React.FC<IGroupMemberProps> = (props) => {

    const dispatch = useDispatch();

    const classes = useStyles();
    const [state, setState] = React.useState({
        access: '',
        name: 'access',
    });
    let GpID = parseInt(props.group_id.toString());
    // useEffect(() => {
    //     dispatch(getAccessThunk(GpID));
    // }, [dispatch, GpID]);

    // //display edit & del
    // const [accessAvailableToShow, setAccessAvailableToShow] = useState(false);
    // // const [accessAvailableToEdit, setAccessAvailableToEdit] = useState(false);
    // const access = useSelector((state: IRootState) => state.groups.access)
    // useEffect(() => {
    //     // console.log(access, `is change???`)
    //     if (access) {
    //         const AccessAvailable = access.access_right
    //         // console.log(`${AccessAvailable} show it now!!!!`);
    //         if (AccessAvailable !== 'reader' || undefined) {
    //             setAccessAvailableToShow(true);
    //             // setAccessAvailableToEdit(true);
    //             console.log('can edit')
    //             return
    //         } else {
    //             setAccessAvailableToShow(false);
    //             // setAccessAvailableToEdit(false);
    //             console.log('cant edit')
    //             return
    //         }
    //     }
    // }, [access])

    const handleChange = async (event: any) => {
        if (props.access !== "reader" || undefined) {
            const name = event.target.name;
            const updateAccess = event.target.value
            setState({
                ...state,
                [name]: updateAccess,
            });
            // console.log(updateAccess);
            const updateMemberID = props.user_id;
            // console.log(updateMemberID);
            // console.log(GpID);
            await dispatch(updateAccessThunk(GpID, updateMemberID, updateAccess));
            await dispatch(getGpDetailAndMembersThunk(GpID));
        }
    };

    const RemoveHandler = async () => {
        // console.log('RemoveHandler work');
        const removeMemberID = props.user_id;
        await dispatch(removeMemberThunk(GpID, removeMemberID));
        await dispatch(getUSearchThunk(GpID));
        await dispatch(getGpDetailAndMembersThunk(GpID));
        await dispatch(getGroupsListThunk());
    }
    return (
        <div className='GroupMember'>
            {props.username}

            {<FormControl
                variant="outlined"
                className={classes.formControl}
            // onChange={() => setAccessAvailableToEdit(true)}
            >
                <InputLabel htmlFor="outlined-age-native-simple">access right</InputLabel>
                <Select
                    native
                    value={props.access_right}
                    onChange={handleChange}
                    label="access right"
                    inputProps={{
                        name: 'access',
                        id: 'outlined-age-native-simple',
                    }}
                >
                    <option aria-label="None" value="" />
                    <option value={'owner'}>owner</option>
                    <option value={'editor'}>editor</option>
                    <option value={'reader'}>reader</option>
                </Select>
            </FormControl>}

            {/* <Button>
                <MoreHorizIcon />
            </Button> */}

            {
                (props.access !== "reader" || undefined) &&
                // accessAvailableToShow &&
                <div className="member-del"
                // onChange={() => setAccessAvailableToShow(true)}
                >
                    <IconButton onClick={RemoveHandler}>
                        <ExitToAppIcon />
                    </IconButton>
                </div>}
        </div>
    )
}

export default GroupMember
