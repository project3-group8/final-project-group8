import React, { useEffect } from 'react';
import '../css/ItemsList.css';
import List from '@material-ui/core/List';

import ItemsListItem from "./ItemsListItem";
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { getExpiryItemsByGroupThunk, getListItemsByUserByGroupByCatThunk, getShortageItemsByGroupThunk } from '../redux/items/thunks';


import { makeStyles, createStyles } from '@material-ui/core/styles';
// import Typography from '@material-ui/core/Typography';
// import Pagination from '@material-ui/lab/Pagination';
import TablePagination from '@material-ui/core/TablePagination';


const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            '& > *': {
                marginTop: theme.spacing(2),
            },
            marginTop: '0px',
            marginBottom: '5px',
            justifyContent: 'center',
        },
    }),
);

// const { REACT_APP_API_SERVER } = process.env;

const ItemsList: React.FC = () => {

    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const dispatch = useDispatch();
    const { category } = useSelector((state: IRootState) => state.items.categoryObj);
    const group_id = useSelector((state: IRootState) => state.items.group_id);
    const listItems = useSelector((state: IRootState) => state.items.listItems);
    const profile = useSelector((state: IRootState) => state.groups.profile);

    let totalItems = 0;
    let totalPages = 1;
    if (listItems && listItems.length > 0 && profile.length > 0) {
        totalItems = listItems.length;
        if (totalItems % rowsPerPage > 0) {
            totalPages = Math.floor(totalItems / rowsPerPage) + 1;
        } else {
            totalPages = totalItems / rowsPerPage;
        }
    }

    let startIdx = rowsPerPage*page;
    let endIdx = startIdx + rowsPerPage - 1;

    useEffect(() => {
        if (group_id !== 0) {
            dispatch(getListItemsByUserByGroupByCatThunk(group_id, category));
            setPage(0);
        }

    }, [dispatch, group_id, category]);


    let alert_quantity = 3; 
    let day_before = 10;     
    useEffect(() => {
        dispatch(getShortageItemsByGroupThunk(group_id, alert_quantity));
        dispatch(getExpiryItemsByGroupThunk(group_id, day_before));
    }, [dispatch, group_id, alert_quantity, day_before]);


    return (
        <div className="ItemsListArea">
            {/* {console.log(listItems)} */}
            {listItems && listItems.length > 0 && profile.length > 0 && (
                <List className="ItemsList">
                    {listItems.map((item, idx) => (
                        ((idx>=startIdx) && (idx<=endIdx)) ?
                        <ItemsListItem
                            key={`item_${idx}`}
                            index_value={idx}
                            item_id={item.item_id}
                            product={item.product}
                            category={item.category}
                            user_id={item.user_id}
                            group_id={item.group_id}
                            expiry_date={item.expiry_date}
                            quantity={item.quantity}
                            image={item.image}
                            inventory_id={item.inventory_id}
                            access={profile.find(profile_ => profile_.group_id === group_id)?.myAccess}
                        /> : null
                    ))}
                </List>
            )}
                        
            <div className={classes.root}>
                {/* <Typography>Page: {page+1}</Typography> */}
                <TablePagination
                    component="div"
                    count={totalItems}
                    page={page}
                    onChangePage={handleChangePage}
                    rowsPerPage={rowsPerPage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </div>
        </div>
    );
};

export default ItemsList;