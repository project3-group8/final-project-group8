import React, { useEffect } from 'react';
import { AppBar, Tabs, Tab, Box } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { getCategoriesByUserByGroupThunk } from '../redux/items/thunks';
import { setCategoryObj } from '../redux/items/actions';
import '../css/CategoryBar.css';
interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
}
const TabPanel = (props: TabPanelProps) => {
    const { children, value, index, ...other } = props;

    return (
        <div className='CategoryBar'
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable_tabpanel-${index}`}
            aria-labelledby={`scrollable_tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <h1>{children}</h1>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: any) {
    return {
        id: `scrollable-auto-tab-${index}`,
        'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
}



const CategoryBar: React.FC = () => {

    const dispatch = useDispatch();
    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };

    const group_id = useSelector((state: IRootState) => state.items.group_id);
    const categories = useSelector((state: IRootState) => state.items.categories);
    const { category } = useSelector((state: IRootState) => state.items.categoryObj);
    // console.log("In CategoryBar");
    // console.log("group_id:", group_id);
    // console.log("categories:", categories)


    const fixCategory = (category: string) => {
        dispatch(setCategoryObj({ category }));
    }

    ////////////////////////////////////////////////////////////////////////////
    // const group_id = 2;

    useEffect(() => {
        // dispatch(getListItemsThunk());
        // console.log("In CategoryBar useEffect");
        // console.log("group_id", group_id);
        dispatch(getCategoriesByUserByGroupThunk(group_id));
        fixCategory('All');
    }, [dispatch, group_id]);

    useEffect(() => {
        if (category === 'All') {
            setValue(0);
        }
    }, [category]);

    return (
        <>
            <AppBar position="static" color="default" className='CategoryBar'>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    aria-label="scrollable tab"
                >
                    <Tab label="All" onClick={() => fixCategory('All')}  {...a11yProps(0)} />
                    {/* <Tab label="cat 1" {...a11yProps(1)} />
                    <Tab label="cat 2" {...a11yProps(2)} />
                    <Tab label="cat 3" {...a11yProps(3)} />
                    <Tab label="cat 4" {...a11yProps(4)} />
                    <Tab label="cat 5" {...a11yProps(5)} />
                    <Tab label="cat 6" {...a11yProps(6)} /> */}
                    {/* <Tab label="cat 7" {...a11yProps(7)} />
                    <Tab label="cat 8" {...a11yProps(8)} /> */}

                    {categories.map((categoryObj, idx) => (
                        <Tab
                            key={`category_${idx}`}
                            label={categoryObj.category}
                            onClick={() => fixCategory(categoryObj.category)}
                            {...a11yProps(idx + 1)}
                        />
                    ))}

                </Tabs>
            </AppBar>
            {/* <TabPanel value={value} index={0}>
                <div>All items here - default select *</div>
            </TabPanel>

            {categories.map((categoryObj, idx) => (
                <TabPanel
                    key={`category_${idx}`}
                    value={value}
                    index={idx + 1}
                >
                    <div>Select item by {categoryObj.category} </div>
                </TabPanel>
            ))} */}


            {/* <TabPanel value={value} index={1}>
                <div>Select item by cat 1</div>
            </TabPanel>
            <TabPanel value={value} index={2}>
                <div>Select item by cat 2</div>
            </TabPanel>
            <TabPanel value={value} index={3}>
                <div>Select item by cat 3</div>
            </TabPanel>
            <TabPanel value={value} index={4}>
                <div>Select item by cat 4</div>
            </TabPanel>
            <TabPanel value={value} index={5}>
                <div>Select item by cat 5</div>
            </TabPanel>
            <TabPanel value={value} index={6}>
                <div>Select item by cat 6</div>
            </TabPanel> */}
        </>
    )
}

export default CategoryBar
