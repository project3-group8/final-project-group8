import React, { useEffect } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Toolbar, createStyles, fade, Theme, makeStyles, CircularProgress, TextField } from '@material-ui/core';

import SearchIcon from '@material-ui/icons/Search';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { getListItemsByUserByGroupByCatThunk, getSearchItemsByGroupByCatThunk, getSearchItemsByUserThunk } from '../redux/items/thunks';
import '../css/ItemSearch.css';

// import { Button } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';


const sleep = (delay = 0) => {
    return new Promise((resolve) => {
        setTimeout(resolve, delay);
    });
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        bar: {
            flexGrow: 1,
            display: 'flex',
            backgroundColor: fade(theme.palette.warning.light, 0.15),
            alignItems: 'center',
            justifyContent: 'center',
            Height: '30px',
        },
        search: {
            position: 'relative',
            borderRadius: theme.shape.borderRadius,
            backgroundColor: fade(theme.palette.common.white, 0.15),
            '&:hover': {
                backgroundColor: fade(theme.palette.common.white, 0.25),
            },
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(1),
                width: 'auto',
            },
        },
        searchIcon: {
            color: '#7c7c7c',
            padding: theme.spacing(0, 2),
            height: '100%',
            position: 'absolute',
            pointerEvents: 'none',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
        inputInput: {
            padding: theme.spacing(1, 2, 1, 0),
            // position: 'relative',
            justifyContent: 'flex-end',
            paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
            transition: theme.transitions.create('width'),
            width: 300,
            [theme.breakpoints.up('sm')]: {
                midWidth: '10%',
                '&:focus': {
                    width: 'auto',
                },
            },
        },
        cirProgress: {
            color: '#ff6721',
            padding: theme.spacing(0, 0, 0, 15),
            // height: '100%',
            // position: 'relative',
            // pointerEvents: 'none',
            // display: 'flex',
            // alignItems: 'center',
            // justifyContent: 'center',
        },

    }),
);
const ItemSearch: React.FC = () => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [options, setOptions] = React.useState([]);

    const loading = open && options.length === 0;

    // let group_id: any;
    // let category: any;
    const group_id = useSelector((state: IRootState) => state.items.group_id);
    const { category } = useSelector((state: IRootState) => state.items.categoryObj);


    const dispatch = useDispatch();
    const items = useSelector((state: IRootState) => state.items.listItems);
    useEffect(() => {
        // console.log("In ItemsSearch useEffect");
        // console.log("group_id", group_id);
        // console.log("category", category);
        dispatch(getListItemsByUserByGroupByCatThunk(group_id, category));
    }, [dispatch, group_id, category]);

    const iSearchList = items.map((item) => (
        { name: item.product }
    ))
    // console.log(iSearchList);
    const [value, setValue] = React.useState(null);
    const [hasText, setHasText] = React.useState(false);

    const clearSearchText = () => {
        setValue(null);
        setHasText(false);        
        if (group_id) {
            dispatch(getListItemsByUserByGroupByCatThunk(group_id, category));
        }
    }

    useEffect(() => {
        let active = true;

        if (!loading) {
            return undefined;
        }

        (async () => {
            await iSearchList;
            await sleep(1e3);
            // const selectedItem = iSearchList.find(item => value = item.name);
            // console.log(selectedItem);
            // dispatch(selectedItem);
            // dispatch(getListItemsByUserByGroupByCatThunk(group_id, category));

            // const countries = await response.json();
            // if (active) {
            //     Object.keys(iSearchList).map((key) => iSearchList[key].item[0]);
            // }
            // const selectedItem = value
            // const selectedItem = iSearchList.find(item => value.name = item.name);
            // console.log(selectedItem)
            // const group_id = localStorage.getItem('group_id');
            // if (!group_id && selectedItem) {
            //     dispatch(getSearchItemsByUserThunk(selectedItem.name));
            // }
            // if (group_id && selectedItem) {
            //     dispatch(getSearchItemsByGroupThunk(Number(group_id), selectedItem.name));
            // }
        })();

        return () => {
            active = false;
        };
    }, [loading]);

    useEffect(() => {
        if (!open) {
            setOptions([]);
        }
    }, [open]);

    return (
        <div className="ItemSearch">

            <Toolbar className={classes.bar}>
                <Autocomplete className={classes.search}
                    id="trial"
                    options={iSearchList}
                    getOptionLabel={(option) => option.name}
                    // getOptionSelected={(option, value) => option.name === value.name}
                    value={value}
                    onChange={(event, value: any) => {
                        setValue(value);
                        // console.log(value)
                        setHasText(true);
                        if (!group_id && value) {
                            dispatch(getSearchItemsByUserThunk(value.name));
                        }
                        if (group_id && value) {
                            // dispatch(getSearchItemsByGroupThunk(Number(group_id), value.name));
                            dispatch(getSearchItemsByGroupByCatThunk(Number(group_id), value.name, category));
                        }
                        else {
                            dispatch(getListItemsByUserByGroupByCatThunk(Number(group_id), category));
                        }
                    }}

                    open={open}
                    onOpen={() => {
                        setOpen(true);
                    }}
                    onClose={() => {
                        setOpen(false);
                    }}
                    loading={loading}
                    renderInput={(params) =>
                        <>
                            <div className={classes.searchIcon}>
                                <SearchIcon />
                            </div>
                            <TextField className={classes.inputInput}
                                {...params}
                                placeholder="Search item…"
                                InputProps={{
                                    ...params.InputProps,
                                    endAdornment: (
                                        <div>
                                            {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                            {/* {params.InputProps.endAdornment} */}
                                            {hasText && <ClearIcon className="clearText-icon" onClick={clearSearchText} />}
                                        </div>
                                    ),
                                }}
                            />
                            {/* <ClearIcon onClick={clearSearchText} /> */}
                            {/* <Button variant="outlined" onClick={clearSearchText}>Reset</Button> */}
                        </>
                    }
                />
            </Toolbar>



        </div>
    )
}

export default ItemSearch;
