import React, { useEffect } from 'react';
import '../css/ItemForm.css';
import { TextField, Button, Checkbox, FormControlLabel } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useForm } from 'react-hook-form';
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';
import ArtTrackIcon from '@material-ui/icons/ArtTrack';
import PostAddIcon from '@material-ui/icons/PostAdd';
// import { BarcodeReader } from '@styled-icons/boxicons-regular';
import { KeyboardDatePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns"; //need to keep this here for the DatePicker
import { useState } from 'react';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { push } from 'connected-react-router';
import { getCategoriesByUserByGroupThunk, getListItemsByUserByGroupByCatThunk } from '../redux/items/thunks';

const { REACT_APP_API_SERVER } = process.env;

interface IItemFormInput {
    product: string;
    category: string;
    expiry_date: Date;
    quantity: number;
    image: FileList;
}

const ItemForm: React.FC = () => {
    const dispatch = useDispatch();
    const [value, setValue] = React.useState(null);
    const categories = useSelector((state: IRootState) => state.items.categories);
    // console.log(categories);
    const categoryList = categories.map((categoryObj) => (
        { category: categoryObj.category }
    ))
    // console.log(categoryList);

    const [selectedDate, setSelectedDate] = useState<Date | null>(
        new Date());
    // console.log(selectedDate)
    // console.log(selectedDate, 2)
    const [objectURL, setObjectURL] = useState("");

    const handleDateChange = (date: Date | null) => {
        // console.log(date, 1);
        // console.log(selectedDate, 3);//cant change
        setSelectedDate(date);
    };
    const [checked, setChecked] = React.useState(false);
    const checkedChange = (event: any) => {
        setChecked(event.target.checked);
    };


    ////////////////////////////////////////////////////////////////////////////////////
    // const group_id = 2;     // hardcore temporarily; should be obtained from redux store
    const group_id = useSelector((state: IRootState) => state.items.group_id);

    const { register, handleSubmit } = useForm<IItemFormInput>();

    const imgPreview = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files && e.target.files.length !== 0 ) {
            setObjectURL(URL.createObjectURL(e.target.files[0]));
        }
    }

    const onSubmit = async (data: IItemFormInput) => {
        // console.log(data.expiry_date)// undefined
        // console.log(typeof data.expiry_date)// undefined
        // console.log(data);
        // console.log(
        //     {
        //         ...data,
        //         group_id,
        //     }
        // );
        // if (!isLoginProcessing) {
        //     dispatch(loginThunk(data.username, data.password));
        // }
        // console.log(setSelectedDate(selectedDate));
        const itemFormData = new FormData();
        const dateString = moment(selectedDate).format('YYYY-MM-DD');
        // console.log(dateString);
        // console.log(typeof dateString);
        const expiryInput = (dateString: any) => {
            if (checked) {
                const XpiredDate = '3900-01-01';
                // console.log(XpiredDate);
                return XpiredDate;
            } else {
                // console.log(dateString.toString());
                return dateString.toString();
            }
        }

        itemFormData.append("image", data.image[0]);
        itemFormData.append("product", data.product);
        itemFormData.append("category", data.category);
        itemFormData.append("expiry_date", expiryInput(dateString));
        itemFormData.append("quantity", data.quantity.toString());
        itemFormData.append("group_id", group_id.toString());
        // console.log(dateString.toString());
        const token = localStorage.getItem('token');
        if (token) {
            const res = await fetch(`${REACT_APP_API_SERVER}/items/createItemAndInvAndTrx`, {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                body: itemFormData
            })

            // if (res.status === 401) {
            //     console.log("Message 401 for ItemForm.tsx");
            // }
            if (res.status === 200) {
                const result = await res.json();
                // console.log("New Item and/or inventory created", result);
                await dispatch(getListItemsByUserByGroupByCatThunk(group_id, data.category)) //cant load the item in that category
                await dispatch(push('/'))
            }
        // } else {
        //     console.log("No token for ItemForm.tsx")
        }
    };

    useEffect(() => {
        // dispatch(getListItemsThunk());
        // console.log("In CategoryBar useEffect");
        // console.log("group_id", group_id);
        dispatch(getCategoriesByUserByGroupThunk(group_id));
    }, [dispatch, group_id]);

    return (
        <div className="ItemForm">
            <h3><PostAddIcon fontSize="large" /> Record Detail</h3>
            <hr />
            <form className="formBody" onSubmit={handleSubmit(onSubmit)}>
                {/* <h2>Input Item</h2> */}

                <TextField
                    className="ItemForm_product"
                    required
                    id="product"
                    name='product'
                    type='string'
                    label="Product"
                    variant="outlined"
                    fullWidth
                    margin='normal'
                    inputRef={register}
                />
                {/* <Button className="BarcodeReader">
                    <BarcodeReader size="50" />
                </Button> */}

                {/* <TextField
                    className="category"
                    id="category"
                    name='category'
                    type='string'
                    label="Category"
                    variant="outlined"
                    fullWidth margin='normal'
                    inputRef={register}
                /> */}
                {/* 
                <label>Category</label>
                <input className="category" id="category" name='category' type="text" list="categoryList" ref={register}/>
                <datalist id="categoryList">
                    {categories.map((categoryObj, idx) => (
                        <option 
                            key={`category_${idx}`}
                            value={categoryObj.category}
                        />
                    ))}
                </datalist> */}

                <Autocomplete
                    freeSolo
                    className="ItemForm_categoryArea"
                    id="categoryArea"
                    options={categoryList}
                    value={value}
                    getOptionLabel={(option: any) => option.category}
                    onChange={(event, value: any) => {
                        setValue(value);
                        // console.log(value);
                    }}
                    renderInput={(params: any) =>
                        <>
                            <TextField className='ItemForm_category'
                                {...params}
                                required
                                id="category"
                                name='category'
                                label="Category"
                                type='string'
                                variant="outlined"
                                fullWidth margin='normal'
                                inputRef={register}
                            />
                        </>
                    }
                />

                {/* <TextField
                    className="expiry_date"
                    id="expiry_date"
                    name='expiry_date'
                    type='date'
                    label="Expiry Date"
                    variant="outlined"
                    fullWidth
                    margin='normal'
                    inputRef={register}
                /> */}
                <div className="ItemForm_expiry_date">
                    <KeyboardDatePicker
                        disableToolbar
                        // className="ItemForm_expiry_date"
                        disabled={checked}
                        required
                        id="expiry_date"
                        name='expiry_date'
                        label="Expiry Date"
                        variant="inline"
                        inputVariant="outlined"
                        format="yyyy-MM-dd"
                        margin="normal"
                        value={selectedDate}
                        onChange={handleDateChange}
                        KeyboardButtonProps={{
                            "aria-label": "change date",
                        }}
                    />
                    <FormControlLabel
                        className="ItemForm_noExpiry_checkbox"
                        control={
                            <Checkbox
                                checked={checked}
                                onChange={checkedChange}

                                name="NoExpiry"
                                // color="secondary"
                                inputProps={{ 'aria-label': 'no expiry date checkbox' }}
                            />
                        }
                        label="No Expiry Date"
                    />
                </div>


                <TextField
                    className="ItemForm_quantity"
                    required
                    id="quantity"
                    name='quantity'
                    type='number'
                    label="Quantity"
                    variant="outlined"
                    fullWidth
                    margin='normal'
                    inputProps={{ min: 0 }}
                    inputRef={register}
                />
                {objectURL && <img src={objectURL} className="imgPreview" />}

                <Button
                    className="ItemForm_upload"
                    variant="outlined"
                    component="label"
                >
                    Upload Image
                    <input
                        name="image"
                        type="file"
                        onChange={(e) => { imgPreview(e) }}
                        ref={register}
                        style={{ display: "none" }}
                    />
                    <AddPhotoAlternateIcon />
                </Button>

                <Button
                    className="ItemForm_submit"
                    type='submit'
                    variant="outlined">
                    <ArtTrackIcon fontSize="large" />
                    Save
                    </Button>
                {/* <hr /> */}
                <Button
                    className="ItemFormReturn"
                    variant="outlined"
                    onClick={() => {
                        dispatch(push('/'));
                    }}
                >
                    RETURN
            </Button>
            </form>
        </div>
    )
}

export default ItemForm;