import React from 'react';
import '../css/ItemUpdateQtyForm.css';
import { TextField, Button, IconButton } from '@material-ui/core';
import { useForm } from 'react-hook-form';
// import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';
import ArtTrackIcon from '@material-ui/icons/ArtTrack';
import PostAddIcon from '@material-ui/icons/PostAdd';
// import { BarcodeReader } from '@styled-icons/boxicons-regular';
// import { KeyboardDatePicker } from "@material-ui/pickers";
// import DateFnsUtils from "@date-io/date-fns"; //need to keep this here for the DatePicker
import { useState } from 'react';
// import moment from 'moment';
import { IListItemProps } from './ItemsListItem';
import { useDispatch } from 'react-redux';
import { updateQtyItemThunk } from '../redux/items/thunks';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';

interface IItemUpdateQtyFormProps {
    closeModal: () => void;
    props: IListItemProps;
}

// const { REACT_APP_API_SERVER } = process.env;

interface IItemUpdateQtyFormInput {
    // product: string;
    // category: string;
    // expiry_date: Date;
    quantity: number;
    // image: FileList;
}

const ItemUpdateQtyForm: React.FC<IItemUpdateQtyFormProps> = ({ closeModal, props }) => {

    // const group_id = 2;     // hardcore temporarily; should be obtained from redux store

    const [quantity, setQuantity] = useState(props.quantity);
    const { register, handleSubmit } = useForm<IItemUpdateQtyFormInput>();
    const dispatch = useDispatch();

    const onSubmit = async (data: IItemUpdateQtyFormInput) => {

        const updated_quantity = Number(data.quantity) - props.quantity;
        // console.log("updated_quantity", updated_quantity);
        // console.log("typeof data.quantity", typeof data.quantity);
        // console.log("props in onSubmit of Form", props);
        const updatedData = {
            ...props,
            quantity: Number(data.quantity),
            updated_quantity,
        }
        // console.log("updatedData", updatedData);
        dispatch(updateQtyItemThunk(updatedData));
        //need to close the modal
        // const ItemUpdateQtyModal: React.FC<IItemUpdateQtyModalProps> = ({ closeModal, props }) => {
        closeModal();
    };

    const increment = () => {
        setQuantity(quantity + 1);
    }

    const decrement = () => {
        if (quantity >= 0) {
            quantity === 0 ? setQuantity(0) : setQuantity(quantity - 1);
        }

    }

    return (
        <div className="ItemUpdateQtyForm">
            <h3><PostAddIcon fontSize="large" /> Record Detail</h3>
            <hr />
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className='middle'>
                    <IconButton onClick={increment} edge="end" aria-label="add">
                        <AddCircleOutlineIcon />
                    </IconButton>

                    <TextField
                        className="quantity"
                        id="quantity"
                        name='quantity'
                        type='number'
                        label="Quantity"
                        variant="outlined"
                        // fullWidth
                        // margin='normal'
                        inputProps={{ min: 0 }}
                        value={quantity}
                        onChange={(e) => { setQuantity(Number(e.target.value)) }}
                        inputRef={register}
                    />

                    <IconButton onClick={decrement} edge="end" aria-label="remove">
                        <RemoveCircleOutlineIcon />
                    </IconButton>
                </div>

                <Button
                    className="ItemUpdateQtyForm_submit"
                    type='submit'
                    variant="outlined"
                >
                    <ArtTrackIcon fontSize="large" />
                    Save
                    </Button>
            </form>

        </div>
    )
}

export default ItemUpdateQtyForm;