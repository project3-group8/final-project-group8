import React from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { IListItemProps } from "./ItemsListItem";
import '../css/ItemImageModal.css';
import { Button } from '@material-ui/core';

const { REACT_APP_API_SERVER_IMAGE } = process.env;

interface IItemImageModalProps {
    closeModal: () => void;
    props: IListItemProps;
}

const ItemImageModal: React.FC<IItemImageModalProps> = ({ closeModal, props }) => {
    // console.log("Props", props);

    const displayURL = props.image;
    // function validURL(displayURL: any) {
    //     var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
    //         '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    //         '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    //         '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    //         '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    //         '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    //     if (pattern.test(displayURL)) {
    //         return displayURL;
    //     } else {
    //         const filePath = `http://localhost:8080/${props.image}`
    //         return filePath;
    //     }
    // }

    function validURL2(displayURL: any) {
        if (displayURL.substring(0, 4) === 'http') {
            return displayURL;
        } else {
            // console.log(`image: ${REACT_APP_API_SERVER_IMAGE}/${props.image}`)
            // return `${REACT_APP_API_SERVER_IMAGE}/${props.image}`;
            return `https://project3-group8-uploads.s3-ap-southeast-1.amazonaws.com/${props.image}`;
            // for local: return `http://localhost:8080/${props.image}`
            // for EC2: return `https://api.groctake.life/${props.image}`
        }
    }

    return (
        <div>
            <Modal isOpen={true} >
                <ModalHeader >Image</ModalHeader>
                <ModalBody className="imgModal" >

                    <img src={validURL2(displayURL)} className="imgDetail" />
                    {/* <img src={`http://localhost:8080/${props.image}`} className="imgDetail"/> */}
                </ModalBody>
                <ModalFooter>
                    <Button
                        className="ImageReturn"
                        onClick={closeModal}
                    >
                        RETURN
                        </Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default ItemImageModal;