
import React, { useEffect, useState } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Toolbar, makeStyles, createStyles, Theme, IconButton } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { getUSearchThunk, addMembersThunk, getGpDetailAndMembersThunk, getGroupsListThunk } from '../redux/groups/thunks';

interface IGroupUserSearchProps {
    group_id: number;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        bar: {
            alignItems: 'center',
            justifyContent: 'center',
            // Height: '20px',
            // width: '100px'
        },
        search: {
            [theme.breakpoints.up('md')]: {
                marginLeft: theme.spacing(1),
                minWidth: '50%',
                width: 'auto',
                // Height: '20px',
            },
            '& > * + *': {
                marginTop: theme.spacing(3),
            },
        },
    }),
);


const MemberSearch: React.FC<IGroupUserSearchProps> = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    let GpID = parseInt(props.group_id.toString());
    const users = useSelector((state: IRootState) => state.groups.users);
    useEffect(() => {
        dispatch(getUSearchThunk(GpID));
        dispatch(getGpDetailAndMembersThunk(GpID));
    }, [dispatch, GpID]);

    const uSearchList = users.map((user) => (
        {
            user_id: user.user_id,
            name: user.username
        }
    ))
    const [value, setValue] = useState<any[]>([]);
    // const setValueArr = (event: any, value: any) => {
    // console.log('selectedHandler work');
    //     console.log(value);
    //     console.log(value.map((user: { user_id: any; }) => ({ user_id: user.user_id })))
    // }

    const addMembersHandler = async (e: any) => {
        // console.log('addMembersHandler work');
        // const selected = [
        //     { 'user_id': 3, 'username': 'Alex' },
        //     { 'user_id': 6, 'username': 'Andrew' },
        //     { 'user_id': 5, 'username': 'Jason' }
        // ];
        // const select = selected.map((user: { user_id: any; }) => ({ user_id: user.user_id }))
        // console.log(select);
        await dispatch(addMembersThunk(GpID, value as []));
        //reset TextField??!!
        // e.preventDefault();
        setValue([]);
        await dispatch(getUSearchThunk(GpID));
        await dispatch(getGpDetailAndMembersThunk(GpID));
        await dispatch(getGroupsListThunk());
    }


    return (
        <div className="MemberSearch">
            <Toolbar className={classes.bar}>
                <Autocomplete className={classes.search}
                    multiple
                    limitTags={2}
                    id="multiple-limit-tags"
                    options={uSearchList}
                    getOptionLabel={(option: any) => option.name}
                    // getOptionSelected={(option: any) => option.user_id}
                    // getOptionSelected={(option, value) => option.label === value}
                    value={value}
                    onChange={(event, value) => setValue(value)}
                    renderInput={(params: any) => (
                        <TextField
                            {...params}
                            variant="outlined"
                            label="participants"
                            placeholder="search by username"
                        />
                    )}
                />
                <IconButton onClick={addMembersHandler}>
                    <EmojiPeopleIcon fontSize="large" />
                </IconButton>
            </Toolbar>
        </div>
    )
}

export default MemberSearch;
