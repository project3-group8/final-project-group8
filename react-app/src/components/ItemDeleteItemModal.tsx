import React from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { IListItemProps } from "./ItemsListItem";
// import ItemUpdateQtyForm from "./ItemUpdateQtyForm";
import '../css/ItemDeleteItemModal.css';
import { useDispatch } from "react-redux";
import { deleteItemThunk } from "../redux/items/thunks";
import { Button } from '@material-ui/core';


interface IItemDeleteItemModalProps {
    closeModal: () => void;
    props: IListItemProps;
}

const ItemDeleteItemModal: React.FC<IItemDeleteItemModalProps> = ({ closeModal, props }) => {
    // console.log("Props", props);
    
    const dispatch = useDispatch();
    const deleteItem = () => {
        // console.log("props in deleteItem, index_value", props.index_value);
        // console.log("props in deleteItem", props);
        dispatch(deleteItemThunk(props));
        closeModal();
    }

    return (
        <div>
            <Modal isOpen={true} >
                <ModalHeader >Delete Item</ModalHeader>
                <ModalBody>
                    {/* <ItemUpdateQtyForm props={props} /> */}
                    <div className="ItemDeleteItemModal" >
                        <Button className="DeleteItemButton" onClick={deleteItem} variant="outlined">
                            {/* <RemoveCircleOutlineIcon /> */}
                            Confirm To Delete Item
                        </Button>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button
                        className="DeleteItemReturn"
                        // color="primary"
                        // variant="outlined"
                        onClick={closeModal}
                    >
                        RETURN
                        </Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default ItemDeleteItemModal;