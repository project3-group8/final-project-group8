import React from 'react';
import { Button } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';
import { setCategoryObj } from '../redux/items/actions';
import { IRootState } from '../redux/store';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import '../css/NotifyPage.css';
import moment from 'moment';


const NotifyPage: React.FC = () => {
    const dispatch = useDispatch();
    const shortageItems = useSelector((state: IRootState) => state.items.shortageItems);
    const expiryItems = useSelector((state: IRootState) => state.items.expiryItems);

    // console.log("shortageItems in NotifyPage:", shortageItems);

    return (
        <div className="NotifyPage">
            <div><h1>Notifications</h1></div>
            <List className="NotifyList">
                {expiryItems.length !== 0 && (<div className="area">
                    {expiryItems.map((item, idx) => (
                        <ListItem key={`item_${idx}`} className='expiryItem'>
                            <ListItemText primary={`${item.product} has ${item.quantity} left and expiry ${moment(item.expiry_date).endOf('hour').fromNow()}`}
                                secondary={item.category} />
                        </ListItem>
                    ))}
                </div>)}
                {shortageItems.length !== 0 && (<div className="area">
                    {shortageItems.map((item, idx) => (
                        <ListItem key={`item_${idx}`} className='shortageItem'>
                            <ListItemText primary={`${item.product} has only ${item.sum} left`} secondary={item.category} />
                        </ListItem>
                    ))}
                </div>)}
            </List>
            <br></br>
            <Button
                className="NotifyPageReturn"
                onClick={() => {
                    dispatch(push('/'));
                    dispatch(setCategoryObj({ category: "All" }));
                }}
            >
                RETURN
                </Button>
        </div>
    );
}

export default NotifyPage;