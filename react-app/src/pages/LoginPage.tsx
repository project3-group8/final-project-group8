import React from 'react';
import '../css/LoginPage.css';
import { TextField, Button } from '@material-ui/core';
import { useForm } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../redux/store';

import { loginThunk } from '../redux/auth/thunks';
import longImg from '../long.png';
import { push } from 'connected-react-router';
// import Alert from '@material-ui/lab/Alert';
import { Alert } from 'reactstrap';
import { loginFail } from '../redux/auth/actions';

interface IFormInput {
    username: string;
    password: string;
}

const LoginPage: React.FC = () => {
    const dispatch = useDispatch();

    // const errMessage = useSelector((state: IRootState) => state.auth.errMessage);
    const isLoginProcessing = useSelector((state: IRootState) => state.auth.isProcessing);

    let errMessage = useSelector((state: IRootState) => state.auth.errMessage);
    // console.log("errMessage", errMessage);

    const { register, handleSubmit } = useForm<IFormInput>();

    const onSubmit = (data: IFormInput) => {
        // console.log(data);
        if (!isLoginProcessing) {
            dispatch(loginThunk(data.username, data.password));
            // console.log("errMessage (after submit login)", errMessage);
        }        
    };

    return (
        <>
            {/* {console.log('login page...')} */}
            <div className="LoginPage">
                <img className="LoginLogo"
                    src={longImg}>
                </img>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <TextField
                        id="username"
                        required
                        name='username'
                        type='text'
                        label="username"
                        variant="outlined"
                        fullWidth margin='normal'
                        inputRef={register}
                    />
                    <TextField
                        id="password"
                        required
                        name='password'
                        type='password'
                        label="password"
                        variant="outlined"
                        fullWidth margin='normal'
                        inputRef={register}
                    />
                    <Button
                        className="SignIn"
                        type='submit'
                        variant="outlined">
                        Login
                    </Button>

                    <hr />

                    <Button
                        className="SignUp"
                        variant="outlined"
                        onClick={() => {
                            dispatch(loginFail(''));
                            dispatch(push('/reg'));
                        }}
                    >
                        Sign Up
                    </Button>
                    {errMessage==="Wrong Username/Password" && <Alert color="danger">Invalid Username/Password</Alert>}
                    {errMessage==="Missing Username/Password" && <Alert color="warning">Missing Username/Password</Alert>}
                </form>
                
            </div>
        </ >
    );
};

export default LoginPage;
