import React from 'react';
import '../css/RegisterPage.css';
import { TextField, Button } from '@material-ui/core';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

import { IRootState } from '../redux/store';
import { RegisterThunk, loginThunk, restoreLoginThunk } from '../redux/auth/thunks';
import { push } from 'connected-react-router';
import { Alert } from 'reactstrap';

interface IRegFormInput {
    username: string;
    password: string;
}

const RegisterPage: React.FC = () => {
    const dispatch = useDispatch();

    // const errMessage = useSelector((state: IRootState) => state.auth.errMessage);
    const isLoginProcessing = useSelector((state: IRootState) => state.auth.isProcessing);

    let errMessage = useSelector((state: IRootState) => state.auth.errMessage);

    const { register, handleSubmit } = useForm<IRegFormInput>();

    const onSubmit = async (data: IRegFormInput) => {
        // console.log(data);        
        await dispatch(RegisterThunk(data.username, data.password));
        const token = localStorage.getItem('token');
        if (!isLoginProcessing && token) {
            await dispatch(loginThunk(data.username, data.password));
        }
    };

    return (
        <div className="RegisterPage">
            <form onSubmit={handleSubmit(onSubmit)}>
                <TextField
                    id="username"
                    required
                    name='username'
                    type='text'
                    label="username"
                    variant="outlined"
                    fullWidth margin='normal'
                    inputRef={register}
                />
                <TextField
                    id="password"
                    required
                    name='password'
                    type='password'
                    label="password"
                    variant="outlined"
                    fullWidth margin='normal'
                    inputRef={register}
                />
                <Button
                    className="RegisterPage_submit"
                    type='submit'
                    variant="outlined">
                    Register
                    </Button>

                <hr />

                <Button
                    className="RegisterPageReturn"
                    variant="outlined"
                    onClick={() => {
                        dispatch(push('/login'));
                    }}
                >
                    RETURN
                </Button>
                {errMessage === "internal server error" && <Alert color="danger">Please use another username</Alert>}
                {errMessage === "Wrong Username/Password" && <Alert color="danger">Please use another username</Alert>}
            </form>

        </div>

    );
};

export default RegisterPage;
