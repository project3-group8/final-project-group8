// import { catchClause } from '@babel/types';
import { push } from 'connected-react-router';
import React, { useEffect, useState } from 'react';
import { Bar } from 'react-chartjs-2';
import { useDispatch, useSelector } from 'react-redux';
import { Button } from '@material-ui/core';
import { loginFail } from '../redux/auth/actions';
import { setCategoryObj } from '../redux/items/actions';
import { IRootState } from '../redux/store';
import '../css/ChartPage.css';

const { REACT_APP_API_SERVER } = process.env;

interface IChartItem {
    item_id: number;
    product: string;
    category: string;
    sum: string;
}

const ChartPage: React.FC = () => {
    const dispatch = useDispatch();
    const group_id = useSelector((state: IRootState) => state.items.group_id);
    const profile = useSelector((state: IRootState) => state.groups.profile);
    const filterGroup = profile.find(item => item.group_id === group_id);
    const { category } = useSelector((state: IRootState) => state.items.categoryObj);

    let data_item_id: Array<number> = [];
    let data_product: Array<string> = [];
    let data_category: Array<string> = [];
    let data_quantity: Array<number> = [];
    const [data, setData] = useState<{ ids: Array<number>, name: Array<string>, category: Array<string>, qty: Array<number> } | undefined>()

    useEffect(() => {

        const getChartData = async () => {
            const token = localStorage.getItem('token');
            if (!token) {
                dispatch(loginFail(''));
            }

            const res = await fetch(`${REACT_APP_API_SERVER}/items/getChartItemsByGroupByCat`, {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    group_id: group_id,
                    category: category,
                }),
            });

            if (res.status === 401) {
                dispatch(loginFail(''));
            }
            if (res.status === 200) {
                const dataObj = await res.json();

                const data = dataObj.chartItems;
                // console.log('ChartItems:', data);

                data_item_id = data.map((x: IChartItem) => x.item_id);
                data_product = data.map((x: IChartItem) => x.product);
                data_category = data.map((x: IChartItem) => x.category);
                data_quantity = data.map((x: IChartItem) => Number(x.sum));

                // console.log('Item_id:', data_item_id);
                // console.log('Quantity:', data_quantity);

                setData({ ids: data_item_id, name: data_product, category: data_category, qty: data_quantity })

            }
        }

        getChartData();
    }, [dispatch])

    const data2 = {
        labels: data?.name,
        datasets: [
            {
                label: `Quantity of each item in this Group ${filterGroup?.group_name} and Category ${category}`,
                // label: `Quantity of each item in Group ${group_id}`,
                data: data?.qty,
                borderColor: 'rgba(54,162,235,1)',
                backgroundColor: 'rgba(54,162,235,0.2)',
                hoverBackgroundColor: 'rgba(255,130,76,0.2)'
            }
        ]
    };

    const options = {
        title: {
            display: false,
            text: 'Bar Chart',
        },
        scales: {
            xAxes: [{
                ticks: {
                    autoSkip: false,
                    maxRotation: 90,
                    minRotation: 85
                }
            }],
            yAxes: [
                {
                    ticks: {
                        min: 0,
                    }
                }
            ]
        }
    };

    return (
        <div className='ChartPage'>
            <div><h1>Report</h1></div>
            <div className='ChartArea'>
                <div className='ChartItem'>
                    {data && <Bar data={data2} options={options} />}
                </div>

                {/* <div className='ChartItem'>
                    {data && <Bar data={data2} options={options} />}
                </div>

                <div className='ChartItem'>
                    {data && <Bar data={data2} options={options} />}
                </div>

                <div className='ChartItem'>
                    {data && <Bar data={data2} options={options} />}
                </div> */}
            </div>

            <br></br>
            <Button
                className="ChartPageReturn"
                onClick={() => {
                    dispatch(push('/'));
                    dispatch(setCategoryObj({ category: "All" }));
                }}
            >
                RETURN
                </Button>
        </div>
    )
}

export default ChartPage;