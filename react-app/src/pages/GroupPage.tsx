import React, { useEffect } from 'react'
import '../css/GroupPage.css';
import GroupList from '../components/GroupList'
import MemberSearch from '../components/MemberSearch'
// import PersonAddIcon from '@material-ui/icons/PersonAdd';
import { Button } from '@material-ui/core';
import ShareIcon from '@material-ui/icons/Share';
import useReactRouter from "use-react-router";
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { getGpDetailAndMembersThunk } from '../redux/groups/thunks';
import { IGroup, IParticipant } from '../redux/groups/state';
// import CallMissedIcon from '@material-ui/icons/CallMissed';
// import CallMissedOutgoingIcon from '@material-ui/icons/CallMissedOutgoing';
// import { setGroupId } from '../redux/items/actions';
// import { getDefaultGroupByTokenThunk, getListItemsByUserByGroupByCatThunk, getListItemsThunk } from '../redux/items/thunks';
// import { push } from 'connected-react-router';


const GroupPage: React.FC<IGroup> = () => {
    const params = useReactRouter().match.params as { id: number };
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getGpDetailAndMembersThunk((params.id)));
        // dispatch(getGroupsListThunk())
    }, [dispatch, params.id]);

    //For using getGroupsListThunk()
    // const profile = useSelector((state: IRootState) => state.groups.profile);
    // console.log(profile);
    // const filterGroup = profile.filter(item => item.group_id === parseInt(params.id.toString()));
    // const filterGroup = profile.find(item => item.group_id === parseInt(params.id.toString()));

    const profile = useSelector((state: IRootState) => state.groups.profile)
    // console.log(profile.find(profile_ => profile_.group_id === parseInt(params.id.toString()))?.myAccess);

    //For using getGpDetailAndMembersThunk()
    const info = useSelector((state: IRootState) => state.groups.info);
    // console.log(info);

    // console.log(params.id);
    // console.log(typeof params.id);

    // console.log(filterGroup);//find
    // console.log(filterGroup?.group_id);//find
    // console.log(filterGroup[0]);//filter


    // const Back_UserIDHandler = async () => {
    //     dispatch(setGroupId(0));
    //     await dispatch(getListItemsThunk());
    //     await dispatch(push('/'));
    // }
    // const Back_GpIDHandler = async () => {
    //     dispatch(setGroupId(params.id));
    //     // console.log(`${params.id} in Back_GpIDHandler`);
    //     // console.log(typeof params.id);
    //     dispatch(getListItemsByUserByGroupByCatThunk(params.id, 'ALL'));
    //     // console.log(`parseInt(params.id.toString() 'after dispatch'`);
    //     await dispatch(push('/'));
    // }

    // //display edit & del
    // const access = useSelector((state: IRootState) => state.groups.access)
    // // const access = useSelector((state: IRootState) => state.groups.profile)
    // // console.log(access);
    // // console.log(access.access_right);
    // useEffect(() => {
    //     dispatch(getAccessThunk(params.id));
    // }, [dispatch, params.id]);

    return (
        <div className="GroupPage">
            {/* <div className='row upper'>
            <Button
                    className="Back-UserID"
                    type='button'
                    variant="outlined"
                    onClick={Back_UserIDHandler}
                >
                    User Items
             <CallMissedIcon />
                </Button> */}
            {/* <PersonAddIcon fontSize="large" className="PersonAddIcon" /> */}
            <h1>Information</h1>
            <br></br>
            {/* <Button
                    className="Back-GpID"
                    type='button'
                    variant="outlined"
                    onClick={Back_GpIDHandler}
                >
                    <CallMissedOutgoingIcon />
                Group Items
            </Button> 
            </div> */}
            <div className='row middle'>
                {/* <h3 className="GroupName">
                <b>{filterGroup[0].group_name}</b> //filter
                <b>{filterGroup?.group_name}</b> //find
                <b>{info.group_name}</b>
            </h3> */}
                <h5>Share to your group members!</h5>
                <h3>Invitation</h3>

            </div>

            <MemberSearch
                group_id={params.id}
            />
            <GroupList
                // group_id={params.id} //find
                // group_id={filterGroup[0].group_id} //filter
                group_id={info.group_id}
                // members={filterGroup[0].members as Array<IParticipant>} //filter
                // members={filterGroup?.members as Array<IParticipant>} //find
                members={info.members as Array<IParticipant>}
                access={profile.find(profile_ => profile_.group_id === parseInt(params.id.toString()))?.myAccess}
            />
            <br></br>
            {/* <Button
                className="GroupCode"
                type='button'
                variant="outlined"
                // value={filterGroup[0].invitation_code}//filter
                // value={filterGroup?.invitation_code}//find
                value={info.invitation_code}

            >
                <ShareIcon />
             Generate Group Code
            </Button> */}
        </div>
    )
}

export default GroupPage
