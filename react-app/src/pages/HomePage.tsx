import React, { useEffect } from 'react'
import '../css/HomePage.css';
import { Button } from '@material-ui/core'
import EditIcon from '@material-ui/icons/Edit';

import CategoryBar from '../components/CategoryBar'
import ItemSearch from '../components/ItemSearch'
import ItemsList from '../components/ItemsList'
import { push } from 'connected-react-router';
import { useDispatch, useSelector } from 'react-redux';
import { getUsernameByTokenThunk } from '../redux/auth/thunks';
import { getDefaultGroupByTokenThunk } from '../redux/items/thunks';
import { IRootState } from '../redux/store';


const HomePage: React.FC = () => {
    const dispatch = useDispatch();

    const group_id = useSelector((state: IRootState) => state.items.group_id);
    const profile = useSelector((state: IRootState) => state.groups.profile);

    useEffect(() => {
        dispatch(getUsernameByTokenThunk());
        const group_id = localStorage.getItem('group_id');
        if (!group_id) {
            dispatch(getDefaultGroupByTokenThunk());
        }        
    }, [dispatch]);

    return (
        <div className="HomePage">
            <CategoryBar />

            <ItemSearch />
            <ItemsList />

            {/* <div className='AddItemA'> */}
            {(profile.find(profile_ => profile_.group_id === group_id)?.myAccess !== "reader" || undefined) &&      
            <Button
                className="AddItem"
                type='button'
                variant="outlined"
                onClick={() => {
                    dispatch(push('/inputItem'));
                }}
            >
                <EditIcon />
                    Add Item
                    </Button>
            }
            {/* </div> */}

        </div>
    )
}

export default HomePage;
