import express from 'express';

import { usersRoutes } from "./routes/usersRoutes";
import { itemsRoutes } from './routes/itemsRoutes';
import { isLoggedIn } from './main';

export const routes = express.Router();

routes.use("/users", usersRoutes);
routes.use("/items", isLoggedIn, itemsRoutes);