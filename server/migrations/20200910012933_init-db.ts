import * as Knex from "knex";
import { tables } from "../tables";

export async function up(knex: Knex): Promise<void> {
    async function createTable(
        tableName: string,
        cb: (table: Knex.CreateTableBuilder) => void
    ) {
        if (await knex.schema.hasTable(tableName)) {
            return;
        }
        await knex.schema.createTable(tableName, cb);
    }
    await createTable(tables.USERS, (table) => {
        table.increments();
        table.string('username').notNullable().unique();
        table.string('password').notNullable();
        table.timestamps(false, true);
    });
    await createTable(tables.GROUPS, (table) => {
        table.increments();
        table.string('group_name').notNullable();
        table.string('owner').notNullable();
        table.string('invitation_code');
        table.timestamps(false, true);
    });
    await createTable(tables.GPAccess, (table) => {
        table.increments();
        table.integer('group_id').unsigned();
        table.foreign('group_id').references(`${tables.GROUPS}.id`);
        table.integer('user_id').unsigned();
        table.foreign('user_id').references(`${tables.USERS}.id`);
        table.string('access_right').notNullable();
        table.timestamps(false, true);
    });

    await createTable(tables.ITEMS, (table) => {
        table.increments();
        table.string('product').notNullable();
        table.string('category');
        table.integer('user_id').unsigned();
        table.foreign('user_id').references(`${tables.USERS}.id`);
        table.integer('group_id');
        table.foreign('group_id').references(`${tables.GROUPS}.id`);
        table.timestamps(false, true);
    });
    await createTable(tables.INV, (table) => {
        table.increments();
        table.integer('item_id').unsigned();
        table.foreign('item_id').references(`${tables.ITEMS}.id`);
        table.date('expiry_date').notNullable();
        table.integer('quantity').unsigned();
        table.string('image');
        table.timestamps(false, true);
    });
    await createTable(tables.TRX, (table) => {
        table.increments();
        table.integer('item_id').unsigned();
        table.foreign('item_id').references(`${tables.ITEMS}.id`);
        table.integer('inventory_id').unsigned();
        table.foreign('inventory_id').references(`${tables.INV}.id`);
        table.integer('updated_quantity').unsigned();
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(tables.TRX);
    await knex.schema.dropTableIfExists(tables.INV);
    await knex.schema.dropTableIfExists(tables.ITEMS);
    await knex.schema.dropTableIfExists(tables.GPAccess);
    await knex.schema.dropTableIfExists(tables.GROUPS);
    await knex.schema.dropTableIfExists(tables.USERS);
}

