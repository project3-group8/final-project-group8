import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

const app = express();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

import Knex from 'knex';
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

import { UsersService } from './services/UsersService';
import { UsersController } from './controllers/UsersController';
import { ItemsService } from './services/ItemsService';
import { ItemsController } from './controllers/ItemsController';
import { GroupsService } from './services/GroupsService';
import { GroupsController } from './controllers/GroupsController';

const usersService = new UsersService(knex);
export const usersController = new UsersController(usersService);
const itemsService = new ItemsService(knex);
export const itemsController = new ItemsController(itemsService);
const groupsService = new GroupsService(knex);
export const groupsController = new GroupsController(groupsService);

import { createIsLoggedIn } from "./guards";
export const isLoggedIn = createIsLoggedIn(usersService);

import { routes } from './routes';
const API_VERSION = "/api/v1";
app.use(API_VERSION, routes);

app.get(`${API_VERSION}/test`, isLoggedIn, (req, res) => {
    console.log(req.user);
    res.json({ message: "hello, world" });
});

// app.get('/', (req, res) => {
//     res.json({ message: 'test get' });
// });

// app.post('/', (req, res) => {
//     res.json({ message: 'test post' });
// });

app.use(express.static('uploads'));

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`listening to port: ${PORT}`);
});