import express from "express";
// import { group } from "yargs";
import { usersController } from "../main";
import { isLoggedIn } from '../main';
import { groupsController } from "../main";

export const usersRoutes = express.Router();


usersRoutes.post('/login', usersController.login);
usersRoutes.get('/info', isLoggedIn, usersController.getUserInfo);
usersRoutes.post('/', usersController.register);
usersRoutes.get('/uSearch/:id', isLoggedIn, usersController.userSearch);

usersRoutes.get('/getDefaultGroupByToken', isLoggedIn, groupsController.getDefaultGroupByUsername);
usersRoutes.get('/group', isLoggedIn, groupsController.getUserFromGroups);
usersRoutes.get('/GpInfo/:id', isLoggedIn, groupsController.getGroupInfo);
// usersRoutes.get('/group', isLoggedIn, groupsController.getUserFromAllGroups);
// usersRoutes.get('/GpMembers/:id', isLoggedIn, groupsController.getAllGroupMembers);
usersRoutes.post('/newGp', isLoggedIn, groupsController.newGroup);
usersRoutes.post('/add/:id', isLoggedIn, groupsController.addGroupMembers);
usersRoutes.delete('/remove/:id', isLoggedIn, groupsController.removeGroupMembers);
usersRoutes.get('/:id/access', isLoggedIn, groupsController.getUserAccessFromGroup);
usersRoutes.put('/:id/updateAccess', isLoggedIn, groupsController.updateMemberAccess);