import express from "express";
import { itemsController } from "../main";
import multer from 'multer';
import AWS from "aws-sdk";
import multerS3 from 'multer-s3';

export const itemsRoutes = express.Router();

// const storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//         // cb(null, `${__dirname}/public/uploads`);
//         cb(null, `${__dirname}/../uploads`);
//     },
//     filename: function (req, file, cb) {
//         cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
//     }
// })
// const upload = multer({ storage });


const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: 'ap-southeast-1'
});

const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'project3-group8-uploads',
        metadata: (req,file,cb)=>{
            cb(null,{fieldName: file.fieldname});
        },
        key: (req,file,cb)=>{
            cb(null,`${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
        }
    })
})



itemsRoutes.get('/', itemsController.getListItemsByUser);
itemsRoutes.post('/getShortageItemsByGroup', itemsController.getShortageItemsByGroup);
itemsRoutes.post('/getExpiryItemsByGroup', itemsController.getExpiryItemsByGroup);
itemsRoutes.post('/getChartItemsByGroup', itemsController.getChartItemsByGroup);
itemsRoutes.post('/getChartItemsByGroupByCat', itemsController.getChartItemsByGroupByCat);
itemsRoutes.post('/getSearchItemsByGroup', itemsController.getSearchItemsByGroup);
itemsRoutes.post('/getSearchItemsByGroupByCat', itemsController.getSearchItemsByGroupByCat);
itemsRoutes.post('/getSearchItemsByUser', itemsController.getSearchItemsByUser);
itemsRoutes.post('/getListItemsByUserByGroupByCat', itemsController.getListItemsByUserByGroupByCat);
itemsRoutes.post('/getCategoriesByUserByGroup', itemsController.getCategoriesByUserByGroup);
itemsRoutes.post('/createItemAndInvAndTrx', upload.single('image'), itemsController.createItemAndInvAndTrx);
itemsRoutes.delete('/deleteItemAndInvAndTrx', itemsController.deleteItemAndInvAndTrx);
itemsRoutes.put('/updateQtyItemAndInvAndTrx', itemsController.updateQtyItemAndInvAndTrx);
