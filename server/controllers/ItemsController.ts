import { ItemsService } from "../services/ItemsService";
import { Request, Response } from 'express';

export class ItemsController {
    constructor(private itemsService: ItemsService) { }

    getListItemsByUser = async (req: Request, res: Response) => {

        try {
            // console.log("In getListItemsByUser");
            const userID = req.user?.id;
            let listItems;
            if (userID) {
                listItems = await this.itemsService.getListItemsByUserID(req.user?.id as number);
                // may also need to include group ide 
            }
            // console.log("UserID",userID)
            res.json({ listItems });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "Error in getListItemsByUser of ItemsController" });
        }
    };

    getChartItemsByGroup = async (req: Request, res: Response) => {
        try {
            const user_id = req.user?.id;
            const { group_id } = req.body;
            let chartItems;
            if (user_id) {
                chartItems = await this.itemsService.getChartItemsByGroup(group_id);
            }
            res.json({ chartItems });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "Error in getChartItemsByGroup of ItemsController" });
        }
    };


    getChartItemsByGroupByCat = async (req: Request, res: Response) => {
        try {
            const user_id = req.user?.id;
            const { group_id, category } = req.body;
            let chartItems;
            if (user_id) {
                chartItems = await this.itemsService.getChartItemsByGroupByCat(group_id, category);
            }
            res.json({ chartItems });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "Error in getChartItemsByGroupByCat of ItemsController" });
        }
    }


    getExpiryItemsByGroup = async (req: Request, res: Response) => {
        try {
            const user_id = req.user?.id;
            const { group_id, day_before } = req.body;
            let expiryItems;
            if (user_id) {
                expiryItems = await this.itemsService.getExpiryItemsByGroup(group_id, day_before);
            }
            res.json({ expiryItems });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "Error in getExpiryItemsByGroup of ItemsController" });
        }
    }



    getShortageItemsByGroup = async (req: Request, res: Response) => {
        try {
            const user_id = req.user?.id;
            const { group_id, alert_quantity } = req.body;
            let shortageItems;
            if (user_id) {
                shortageItems = await this.itemsService.getShortageItemsByGroup(group_id, alert_quantity);
            }
            res.json({ shortageItems });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "Error in getShortageItemsByGroup of ItemsController" });
        }
    };

    getSearchItemsByUser = async (req: Request, res: Response) => {
        try {
            const user_id = req.user?.id;
            const { key_word } = req.body;
            console.log("key_word from req.body in getSearchItemsByUser:", key_word);

            let listItems;
            if (user_id) {
                listItems = await this.itemsService.getSearchItemsByUser(user_id, key_word);
            }
            res.json({ listItems });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "Error in getSearchItemsByUser of ItemsController" });
        }
    };

    getSearchItemsByGroup = async (req: Request, res: Response) => {
        try {
            // const user_id = req.user?.id;
            const { group_id, key_word } = req.body;
            console.log("key_word from req.body in getSearchItemsByGroup:", key_word);

            let listItems;
            if (group_id) {
                listItems = await this.itemsService.getSearchItemsByGroup(group_id, key_word);
            }
            res.json({ listItems });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "Error in getSearchItemsByGroup of ItemsController" });
        }
    };


    getSearchItemsByGroupByCat = async (req: Request, res: Response) => {
        try {
            // const user_id = req.user?.id;
            const { group_id, key_word, category } = req.body;
            // console.log("key_word from req.body in getSearchItemsByGroup:", key_word);

            let listItems;
            if (group_id) {
                listItems = await this.itemsService.getSearchItemsByGroupByCat(group_id, key_word, category);
            }
            res.json({ listItems });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "Error in getSearchItemsByGroupByCat of ItemsController" });
        }
    };



    getListItemsByUserByGroupByCat = async (req: Request, res: Response) => {
        try {
            const user_id = req.user?.id;
            const { group_id, category } = req.body;
            console.log("Category from req.body in getListItemsByUserByGroupByCat:", category);

            let listItems;
            if (user_id) {
                listItems = await this.itemsService.getListItemsByUserByGroupByCat(user_id, group_id, category);
            }
            res.json({ listItems });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "Error in getListItemsByUserByGroupByCat of ItemsController" });
        }
    };


    getCategoriesByUserByGroup = async (req: Request, res: Response) => {
        try {
            const user_id = req.user?.id;
            const { group_id } = req.body;

            let categories;
            if (user_id) {
                categories = await this.itemsService.getCategoriesByUserByGroup(user_id, group_id);
            }
            res.json({ categories });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "Error in getCategoriesByUserByGroup of ItemsController" });
        }
    };


    createItemAndInvAndTrx = async (req: Request, res: Response) => {
        try {
            // const image = req.file ? req.file.filename : "noimage1.jpg";
            const image = req.file ? (req.file as any).key : "noimage1.jpg";
            // Nice to include validateRule for the following variables
            const { product, category, expiry_date, quantity, group_id } = req.body;

            const user_id = req.user?.id as number;
            // const group_id = 2;   // hardcode group_id = 2, should come from with req.body
            // console.log(expiry_date, `received in controller`)
            const result = await this.itemsService.createItemAndInvAndTrx(
                product,
                category,
                expiry_date,
                quantity,
                image,
                user_id,
                group_id);

            res.json({
                item_id: result.item_id,
                inventory_id: result.inventory_id,
                transaction_id: result.transaction_id
            });

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "Error in createItemAndInvAndTrx of ItemsController" });
        }
    }

    deleteItemAndInvAndTrx = async (req: Request, res: Response) => {
        try {
            const { item_id, inventory_id } = req.body;
            const message = await this.itemsService.deleteItemAndInvAndTrx(item_id, inventory_id);
            console.log(message);
            if (message == "Delete success") {
                res.status(200).json({ message });
            } else {
                res.status(500).json({ message });
            }

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "Error in deleteItemAndInvAndTrx of ItemsController" });
        }
    }


    updateQtyItemAndInvAndTrx = async (req: Request, res: Response) => {
        try {
            const { item_id, inventory_id, quantity, updated_quantity } = req.body;
            const message = await this.itemsService.updateQtyItemAndInvAndTrx(item_id, inventory_id, quantity, updated_quantity);
            console.log(message);
            if (message == "Update quantity success") {
                res.status(200).json({ message });
            } else {
                res.status(500).json({ message });
            }

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "Error in updateItemAndInvAndTrx of ItemsController" });
        }
    }


    // createItem = async (req: Request, res: Response) => {
    //     try {
    //         {
    //             const validateRule = { product: 'string', category: 'string' };
    //             for (const key in validateRule) {
    //                 if (typeof req.body[key] !== validateRule[key]) {
    //                     res.status(400).json({ message: 'Invalid Input in createItem of ItemsController' });
    //                     return;
    //                 }
    //             }
    //         }

    //         const { product, category } = req.body;
    //         const userID = req.user?.id as number;
    //         const groupID = 2;   // hardcode group_id = 2
    //         const id = await this.itemsService.createItem(product,
    //             category,
    //             userID,
    //             groupID);
    //         res.json({ id }); // return item id
    //     } catch (err) {
    //         console.error(err.message);
    //         res.status(500).json({ message: "Error in createItem of ItemsController" });
    //     }
    // }

    // createInv = async (req: Request, res: Response) => {
    //     try {
    //         {
    //             const validateRule = {
    //                 item_id: 'number',
    //                 expiry_date: 'Date',
    //                 quantity: 'number',
    //                 image: 'string'
    //             };
    //             for (const key in validateRule) {
    //                 if (typeof req.body[key] !== validateRule[key]) {
    //                     res.status(400).json({ message: 'Invalid Input in createInv of ItemsController' });
    //                     return;
    //                 }
    //             }
    //         }

    //         const { item_id, expiry_date, quantity, image } = req.body;
    //         const id = await this.itemsService.createInv(item_id,
    //             expiry_date,
    //             quantity,
    //             image);
    //         res.json({ id }); // return inventory id
    //     } catch (err) {
    //         console.error(err.message);
    //         res.status(500).json({ message: "Error in createInv of ItemsController" });
    //     }
    // }


}