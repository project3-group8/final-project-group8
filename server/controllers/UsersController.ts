import { UsersService } from "../services/UsersService";
import jwtSimple from "jwt-simple";
import jwt from "../jwt";
import { Request, Response } from "express";
import { checkPassword, hashPassword } from "../hash";


export class UsersController {
    constructor(private userService: UsersService) { }
    login = async (req: Request, res: Response) => {
        try {
            if (!req.body.username || !req.body.password) {
                res.status(401).json({ message: "Missing Username/Password" });
                return;
            }
            const { username, password } = req.body;
            const user = await this.userService.getUserByUsername(username);
            if (!user || !(await checkPassword(password, user.password))) {
                res.status(401).json({ message: "Wrong Username/Password" });
                return;
            }
            const payload = {
                id: user.id,
                // username: user.username,
            };

            console.log(payload);

            const token = jwtSimple.encode(payload, jwt.jwtSecret);

            console.log(token);
            res.json({
                token: token,
            });
        } catch (e) {
            console.log(e.message);
            res.status(500).json({ message: "internal server error" });
        }
    };

    getUserInfo = async (req: Request, res: Response) => {
        try {
            const user = req.user;
            res.status(200).json({
                user: {
                    username: user?.username,
                },
            });
        } catch (e) {
            console.log(e.message);
            res.status(500).json({ message: 'UserController - internal server error' });
        }
    };

    register = async (req: Request, res: Response) => {
        try {
            const { username, password } = req.body;
            const passwordEntered = await hashPassword(password)
            const userID = await this.userService.createUser(
                username,
                passwordEntered
            );
            let payload: { id: number };
            payload = { id: userID };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            return res.json({
                token: token,
            })
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };
    userSearch = async (req: Request, res: Response) => {
        try {
            const GpID = parseInt(req.params.id);
            const users = await this.userService.getAllUsersNotInGp(GpID);
            res.json({ users });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

}

