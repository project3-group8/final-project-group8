import { GroupsService } from "../services/GroupsService";
import jwtSimple from "jwt-simple";
import jwt from "../jwt";
import { Request, Response } from "express";


export class GroupsController {
    constructor(private groupService: GroupsService) { }

    getUserFromGroups = async (req: Request, res: Response) => {
        try {
            const userID = req.user?.id as number;
            let groups;
            if (userID) {
                const originalResult = await this.groupService.getGroups(userID);

                let grouped = {};
                for (let item of originalResult) {
                    const { user_id, username, access_right } = item;
                    const member = { user_id, username, access_right }
                    if (grouped[item.group_name]) {
                        grouped[item.group_name].members.push(member);
                    } else {
                        const { group_name, group_id, owner, invitation_code } = item;
                        grouped[item.group_name] = { group_id, group_name, owner, invitation_code };
                        grouped[item.group_name].members = [member];
                    }
                    if (member.user_id === userID) {
                        grouped[item.group_name]['myAccess'] = member.access_right;
                    }
                }
                groups = Object.values(grouped);
            }
            res.json({ groups });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    getGroupInfo = async (req: Request, res: Response) => {
        try {
            const GpID = parseInt(req.params.id);
            let info;
            if (GpID) {
                const originalResult = await this.groupService.getGroupInfoMembersByGpID(GpID);
                let updatedInfo = {};
                for (let item of originalResult) {
                    const { user_id, username, access_right } = item;
                    const member = { user_id, username, access_right }
                    if (updatedInfo[item.group_id]) {
                        updatedInfo[item.group_id].members.push(member);
                    } else {
                        const { group_name, group_id, owner, invitation_code } = item;
                        updatedInfo[item.group_id] = { group_id, group_name, owner, invitation_code };
                        updatedInfo[item.group_id].members = [member];
                    }
                }
                info = Object.values(updatedInfo)
            }
            // console.log(info);
            res.json({ info });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };


    // getUserFromAllGroups = async (req: Request, res: Response) => {
    //     try {

    //         const userID = req.user?.id as number;
    //         let group;
    //         if (userID) {
    //             group = await this.groupService.getGroupsByUserID(userID);
    //         }
    //         res.json({ group });
    //     } catch (err) {
    //         console.error(err.message);
    //         res.status(500).json({ message: 'internal server error' });
    //     }
    // };

    // getGroupInfoOLD = async (req: Request, res: Response) => {
    //     try {
    //         const GpID = parseInt(req.params.id);
    //         console.log(GpID);
    //         let info;
    //         if (GpID) {
    //             info = await this.groupService.getGroupInfoByGpID(GpID);
    //         }
    //         console.log(info);
    //         res.json({ info });
    //     } catch (err) {
    //         console.error(err.message);
    //         res.status(500).json({ message: 'internal server error' });
    //     }
    // };


    // getAllGroupMembers = async (req: Request, res: Response) => {
    //     try {
    //         const GpID = parseInt(req.params.id);
    //         console.log(GpID);
    //         let members;
    //         if (GpID) {
    //             members = await this.groupService.getGpMembersByGpID(GpID);
    //         }
    //         console.log(members);
    //         res.json({ members });
    //     } catch (err) {
    //         console.error(err.message);
    //         res.status(500).json({ message: 'internal server error' });
    //     }
    // };

    newGroup = async (req: Request, res: Response) => {
        try {
            const user_id = req.user?.id as number;
            const existingUser = req.user?.username as string;
            const newGroupName = req.body.group_name;
            let payload: { id: number | undefined };
            let groupID;
            payload = { id: groupID };
            const GPtoken = jwtSimple.encode(payload, jwt.jwtSecret);
            groupID = await this.groupService.createGroup(
                user_id,
                existingUser,
                newGroupName,
                GPtoken
            );
            // console.log(`${user_id} at newGroup groupController`)

            // payload = { id: groupID };
            // console.log(`${payload} at newGroup groupController`)
            // const GPtoken = jwtSimple.encode(payload, jwt.jwtSecret);
            // console.log(`${GPtoken} at newGroup groupController`)
            console.log(`User ${user_id}${existingUser} is successfully created newGP${newGroupName}`);
            // console.log(`${token} at gpController`);
            return res.json({
                group_id: groupID,
                user_id: user_id,
                owner: existingUser,
                token: GPtoken,
            });
            // return res.status(201).json('User is successfully created');
        } catch (err) {
            console.error(err.message);
            return res.status(500).json({ message: "internal server error" });
        }
    };

    addGroupMembers = async (req: Request, res: Response) => {
        try {
            const GpID = parseInt(req.params.id);
            const addMembersArr = req.body.addMembersArr;
            const newMembers = await this.groupService.addUsersNotInGp(
                GpID,
                addMembersArr
            );
            // console.log(newMembers);
            res.json({ newMembers });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    removeGroupMembers = async (req: Request, res: Response) => {
        try {
            const GpID = parseInt(req.params.id);
            const removeMember = req.body.removeMemberID;
            const byeMembers = await this.groupService.removeUsersInGp(
                GpID,
                removeMember
            );
            // console.log(byeMembers);
            res.json({ byeMembers });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    getUserAccessFromGroup = async (req: Request, res: Response) => {
        try {
            const userID = req.user?.id as number;
            const GpID = parseInt(req.params.id);
            const access = await this.groupService.getMemberAccessByGpID(userID, GpID);
            // console.log(access);
            res.json({ access });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };
    updateMemberAccess = async (req: Request, res: Response) => {
        try {
            const GpID = parseInt(req.params.id);
            const updateMemberID = req.body.user_id;
            const updateAccess = req.body.access_right;
            // console.log(GpID, `in updateMemberAccess`);
            // console.log(updateMemberID, `in updateMemberAccess`);
            // console.log(updateAccess, `in updateMemberAccess`);

            const updateMember = await this.groupService.updateMemberAccessInGp(
                GpID,
                updateMemberID,
                updateAccess
            );
            // console.log(updateMember);
            res.json({ updateMember });
        } catch (err) {
            // console.log("Error in updateMember of updateMemberAccess Gp Controller");
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }


    getDefaultGroupByUsername = async (req: Request, res: Response) => {
        try {
            const username = req.user?.username as string;
            const group = await this.groupService.getDefaultGroupByUsername(username)
            res.json({ group });
        } catch (err) {
            res.status(500).json({ message: 'internal server error (from getDefaultGroupByUsername)' });
        }
    }

};

