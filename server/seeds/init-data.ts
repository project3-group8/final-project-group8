import * as Knex from "knex";
import { tables } from "../tables";
import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(tables.TRX).del();
    await knex(tables.INV).del();
    await knex(tables.ITEMS).del();
    await knex(tables.GPAccess).del();
    await knex(tables.GROUPS).del();
    await knex(tables.USERS).del();
    //setup admin first
    const [adminID] = await knex(tables.USERS).insert(
        { username: 'admin', password: await hashPassword('admin'), }
    ).returning('id');

    // Inserts other seed entries
    const teckyIDs = await knex(tables.USERS).insert([
        { username: 'tecky', password: await hashPassword('tecky'), },
        { username: 'Gordon', password: await hashPassword('Gordon'), },
        { username: 'Alex', password: await hashPassword('Alex'), },
        { username: 'Michael', password: await hashPassword('Michael'), },
        { username: 'Jason', password: await hashPassword('Jason'), },
        { username: 'Andrew', password: await hashPassword('Andrew'), },
        { username: 'Lung', password: await hashPassword('Lung'), },
        { username: 'Beeno', password: await hashPassword('Beeno'), },
    ]).returning('id');

    const gpIDs = await knex(tables.GROUPS).insert([
        { group_name: 'admin', owner: 'admin', invitation_code: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MX0.6YSpDOtVdZAYRlIrnWs1FdKeVEw_lBm38BPnn__cXTg', },
        { group_name: 'tecky', owner: 'tecky', invitation_code: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6Mn0.2wuVMbrT7MrQp5G5En90YhLNk2msOpVNj3cczzuke6g', },
        { group_name: 'Gordon', owner: 'Gordon', invitation_code: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6M30.UWoi0TdUmV3Epo2qQk03qv4jabBZRsZJ2IFsjCaVNho', },
        { group_name: 'Alex', owner: 'Alex', invitation_code: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6NH0.E98BmFMdZWOOkIJt2I90ADZSpB1mA7w9Bs8EBW2GaeI', },
        { group_name: 'Michael', owner: 'Michael', invitation_code: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6NX0.8BArczrg4dsiSFA_s5n8RqAmSJacusZo33hd55MolFM', },
        { group_name: 'Jason', owner: 'Jason', invitation_code: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6Nn0.8UMRrg00gPWkTGmxPAjvoxfao2Vz_bxc1StKX7yr7ug', },
        { group_name: 'Andrew', owner: 'Andrew', invitation_code: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6N30.5ZFlg6_0Aa3q34xwj5ocFWtRMeP7JeOK2Y093-Tc9FE', },
        { group_name: 'Lung', owner: 'Lung', invitation_code: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6OH0.dPoClKGu-aCqxqcr5fGjAne4Y5E739Z2hri8VvD_Esg', },
        { group_name: 'Beeno', owner: 'Beeno', invitation_code: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6OX0.Vrbxq3t7is3kWRFRYspC8xOuLyhYG8E2g1aQ4zbYqdU', },
        { group_name: 'X’mas Party Gathering', owner: 'tecky', invitation_code: null, },
    ]).returning('id');

    const OWNER = 'owner';
    const EDITER = 'editor';
    const READER = 'reader';

    await knex(tables.GPAccess).insert([
        //adminOwnGroup
        { group_id: gpIDs[0], user_id: adminID, access_right: OWNER },
        { group_id: gpIDs[0], user_id: teckyIDs[0], access_right: EDITER },
        //teckyOwnGroup          
        { group_id: gpIDs[1], user_id: teckyIDs[0], access_right: OWNER },//tecky
        { group_id: gpIDs[1], user_id: teckyIDs[1], access_right: OWNER },//Gordon
        { group_id: gpIDs[1], user_id: teckyIDs[2], access_right: OWNER },//Alex
        { group_id: gpIDs[1], user_id: teckyIDs[3], access_right: OWNER },//Michael
        { group_id: gpIDs[1], user_id: teckyIDs[4], access_right: EDITER },//Jason
        { group_id: gpIDs[1], user_id: teckyIDs[5], access_right: EDITER },//Andrew
        { group_id: gpIDs[1], user_id: teckyIDs[6], access_right: READER },//Lung
        { group_id: gpIDs[1], user_id: teckyIDs[7], access_right: READER },//Beeno
        //GordonPersonal
        { group_id: gpIDs[2], user_id: teckyIDs[1], access_right: OWNER },//Gordon
        //AlexPersonal
        { group_id: gpIDs[3], user_id: teckyIDs[2], access_right: OWNER },//Alex
        //MichaelPersonal
        { group_id: gpIDs[4], user_id: teckyIDs[3], access_right: OWNER },//Michael
        //JasonPersonal
        { group_id: gpIDs[5], user_id: teckyIDs[4], access_right: OWNER },//Jason
        //AndrewPersonal
        { group_id: gpIDs[6], user_id: teckyIDs[5], access_right: OWNER },//Andrew
        //LungPersonal
        { group_id: gpIDs[7], user_id: teckyIDs[6], access_right: OWNER },//Lung
        //BeenoPersonal
        { group_id: gpIDs[8], user_id: teckyIDs[7], access_right: OWNER },//Beeno
        //2020 X’mas Gathering Preparation
        { group_id: gpIDs[9], user_id: teckyIDs[0], access_right: OWNER },//tecky
        { group_id: gpIDs[9], user_id: adminID, access_right: EDITER },//admin
    ]);

    const FRUITS = 'Fruits';
    const BEVERAGES = 'Beverages';
    const EyeCare = 'Eye Care';
    const GAMING = 'Gaming';
    const KitchenRelated = 'Kitchen Appliances';
    const CameraRelated = 'Camera & Accessories';
    const ComputerRelated = 'Computer & Accessories';
    const STATIONERIES = 'Stationeries';
    const CLEANING = 'Cleaning';
    const SNACKS = 'Snacks';

    const itemIDs = await knex(tables.ITEMS).insert([
        { product: 'Apple', category: FRUITS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Coffee', category: BEVERAGES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Tea', category: BEVERAGES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Lemon', category: FRUITS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Apple', category: FRUITS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Cola', category: BEVERAGES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Tea', category: BEVERAGES, user_id: teckyIDs[4], group_id: gpIDs[1], },//User:Jason,Gp:tecky
        { product: 'Banana', category: FRUITS, user_id: teckyIDs[5], group_id: gpIDs[1], },//User:Andrew,Gp:tecky

        { product: '2-Week Contact Lens', category: EyeCare, user_id: teckyIDs[6], group_id: gpIDs[7], },//User:Lung,Gp:Lung
        { product: 'Switch Console', category: GAMING, user_id: teckyIDs[2], group_id: gpIDs[1], },//User:Alex,Gp:tecky
        { product: 'Switch RingFit', category: GAMING, user_id: teckyIDs[1], group_id: gpIDs[1], },//User:Gordon,Gp:tecky
        { product: '2.2L Air Fryer', category: KitchenRelated, user_id: teckyIDs[2], group_id: gpIDs[3], },//User:Alex,Gp:Alex
        { product: 'Instax Mini Film Pack', category: CameraRelated, user_id: teckyIDs[4], group_id: gpIDs[5], },//User:Jason,Gp:Jason
        { product: 'Anti-fog Glasses Cloth', category: CameraRelated, user_id: teckyIDs[4], group_id: gpIDs[5], },//User:Jason,Gp:Jason
        { product: '7in1 External Card Reader', category: CameraRelated, user_id: teckyIDs[4], group_id: gpIDs[5], },//User:Jason,Gp:Jason
        { product: '9L ABS Dehumidifying Cabinet', category: CameraRelated, user_id: teckyIDs[4], group_id: gpIDs[5], },//User:Jason,Gp:Jason
        { product: 'Aluminum Tripod', category: CameraRelated, user_id: adminID, group_id: gpIDs[0], },//User:admin,Gp:admin
        { product: 'Portable Folding Mini Photography Tool Box', category: CameraRelated, user_id: adminID, group_id: gpIDs[0], },//User:admin,Gp:admin
        { product: '2TB USB 3.0 2.5" Portable HDD', category: ComputerRelated, user_id: adminID, group_id: gpIDs[0], },//User:admin,Gp:admin
        { product: 'AC1200 MU-MIMO Gigabit Router', category: ComputerRelated, user_id: adminID, group_id: gpIDs[0], },//User:admin,Gp:admin
        { product: 'A4 Paper', category: STATIONERIES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Adhesive Type', category: STATIONERIES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Color Whiteboard Marker Set', category: STATIONERIES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: '4" x 9" White Envelope with narrow open', category: STATIONERIES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'MX Vertical Mouse', category: ComputerRelated, user_id: adminID, group_id: gpIDs[0], },//User:admin,Gp:admin
        { product: 'Bluetooth Keyboard', category: ComputerRelated, user_id: adminID, group_id: gpIDs[0], },//User:admin,Gp:admin
        { product: 'Disinfectant Spray', category: CLEANING, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Wet Wipe', category: CLEANING, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: '3-ply Tissue', category: CLEANING, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Patent mop', category: CLEANING, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'All Purpose Gloves', category: CLEANING, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky

        { product: 'Mamee Noodles', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Golden Duck - SALTED EGG CRAB SEAWEED TEMPURA', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Tomato Flavor Potato Chipss', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Red Bull Energy Drink', category: BEVERAGES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Bento - Spicy Squid Seafood Snacks', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Calbee - Spicy Flavour Potato Chips', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Calbee - Grill A Corn Barbecue Flavoured', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Sze Hing Loong - Chilli Corn Roll', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Baby Star - Dodekai Fettuccine Chicken Flavour', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Taiyo - Wiener Sausage', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Pretz Salad', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Strawberry Pocky', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Tomato Pretz', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Calbee - Vegetables Fries', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Lotte - Koala March"s Chocolate Biscuits', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Haribo - Goldbaeren Gummy Mini', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Nabisco - Chips Ahoy Chocolate Chip Cookies', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Vitasoy - Malted Soyabean Milk', category: BEVERAGES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Vita - Lemon Tea', category: BEVERAGES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky

        { product: 'Coca-Cola', category: BEVERAGES, user_id: teckyIDs[0], group_id: gpIDs[9], },//User:tecky,Gp:Xmas
        { product: 'Calbee - Prawn Stick', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[9], },//User:tecky,Gp:Xmas
        { product: 'Calbee - Pizza Flavor Potato Chips Hot & Spicy Thick Cuts Chips', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[9], },//User:tecky,Gp:Xmas

        { product: 'FOUR SEAS - Seaweed Crispy Prawn Crackers', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Korea Seaweed', category: SNACKS, user_id: adminID, group_id: gpIDs[0], },//User:admin,Gp:admin
        { product: 'Calbee - Seaweed Flavour Chips', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'ORIHIRO - Konjac Jelly Peach & Lychee', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'Tao Ti - Supreme Meta Tea (No sugar)', category: BEVERAGES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'CheckCheckCin - Osmanthus & Pear Rice Water', category: BEVERAGES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'HealthWorks - Lemon Yiyiren Drink', category: BEVERAGES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky

        { product: 'SANGARIA Strong bottle green tea', category: BEVERAGES, user_id: teckyIDs[0], group_id: gpIDs[9], },//User:tecky,Gp:Xmas
        { product: 'Lotte - Milkis Original', category: BEVERAGES, user_id: teckyIDs[0], group_id: gpIDs[9], },//User:tecky,Gp:Xmas

        { product: 'Ucc - Blended Coffee', category: BEVERAGES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky
        { product: 'HealthWorks - Hawthorn Apple Herbal Drink', category: BEVERAGES, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky

        { product: 'Oreo - Original Mini Multipack', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[9], },//User:tecky,Gp:Xmas

        { product: 'Oreo - Original Sandwich Biscuits', category: SNACKS, user_id: teckyIDs[0], group_id: gpIDs[1], },//User:tecky,Gp:tecky

        // { product: 'Food', category: SNACKS, user_id: adminID, group_id: gpIDs[0], },//User:admin,Gp:admin

    ]).returning('id');

    const XpiredDate = '3900-01-01';

    const invIDs = await knex(tables.INV).insert([
        //Apple*original hv 1 later TRX +6
        { item_id: itemIDs[0], expiry_date: '2020-10-01', quantity: 7, image: 'https://www.parknshop.com/medias/sys_master/front/prd/8961709047838.jpg', },
        //Coffee
        { item_id: itemIDs[1], expiry_date: '2019-12-01', quantity: 2, image: 'https://images.hktv-img.com/images/HKTV/10868/4191_main_45774339_20200715165545_01_1200.jpg', },
        //Tea
        { item_id: itemIDs[2], expiry_date: '2020-12-01', quantity: 2, image: 'https://www.parknshop.com/medias/sys_master/front/prd/9185776861214.jpg', },
        //Lemon
        { item_id: itemIDs[3], expiry_date: '2020-12-31', quantity: 3, image: 'https://www.parknshop.com/medias/sys_master/front/prd/8883164053534.jpg', },
        //!!Expired Apple
        { item_id: itemIDs[4], expiry_date: '2020-01-13', quantity: 1, image: 'https://fxpro.news/wp-content/uploads/2020/04/apple-logo-rob-janoff-01.jpg', },
        //!!Expired Cola
        { item_id: itemIDs[5], expiry_date: '2019-12-01', quantity: 2, image: 'https://img-9gag-fun.9cache.com/photo/ad9LnXD_700bwp.webp', },
        //!!Expired Tea
        { item_id: itemIDs[6], expiry_date: '2020-06-04', quantity: 2, image: 'https://www.delico.nu/media/catalog/product/cache/3/image/500x/9df78eab33525d08d6e5fb8d27136e95/c/e/celestial_tea_fruit_tea_sampler_18pcs_2017_1.png', },
        //!!Expired Banana
        { item_id: itemIDs[7], expiry_date: '2020-02-16', quantity: 3, image: 'https://www.adventistreview.org/phpthumbsup/w/1000/h/500/zc/1/src/assets/public/features/2019/old-banana.jpg', },
        //2-Week Contact Lens
        { item_id: itemIDs[8], expiry_date: '2022-01-01', quantity: 2, image: 'https://images.hktvmall.com/h5711001/176450/h5711001_oasyspi_reg_2week88100s_180424122318_02_1200.jpg', },
        //Switch Console*香港行貨一年保養
        { item_id: itemIDs[9], expiry_date: '2021-09-13', quantity: 1, image: 'https://images.hktv-img.com/images/HKTV/10823/GA202008011014_main_47113740_20200801102117_01_1200.jpg', },
        //Switch RingFit
        { item_id: itemIDs[10], expiry_date: XpiredDate, quantity: 1, image: 'https://images.hktv-img.com/images/HKTV/10892/NW0007_main_40142355_20200515192427_01_1200.jpg', },
        //Air Fryer*Warranty Period : 12 Months
        { item_id: itemIDs[11], expiry_date: '2021-10-01', quantity: 1, image: 'https://images.hktv-img.com/images/HKTV/15833/KGKZ-2202WE_main_35134422_20200306131454_01_1200.jpg', },
        //Instax Mini Film Pack
        { item_id: itemIDs[12], expiry_date: '2020-12-16', quantity: 1, image: 'https://images.hktvmall.com/h5239001/78066/h5239001_mini_film_twinpack_170125121513_01_1200.jpg', },
        //Anti-fog Glasses Cloth
        { item_id: itemIDs[13], expiry_date: XpiredDate, quantity: 1, image: 'https://images.hktv-img.com/images/HKTV/13622/A0152_main_40300881_20200519100647_01_1200.jpg', },
        //7in1 External Card Reader*Warranty Period : 12 Months
        { item_id: itemIDs[14], expiry_date: '2021-12-01', quantity: 1, image: 'https://images.hktvmall.com/s1116001/193873/s1116001_4820_180611101736_01_1200.jpg', },
        //9L ABS Dehumidifying Cabinet*Warranty Period : 12 Months
        { item_id: itemIDs[15], expiry_date: '2021-12-01', quantity: 1, image: 'https://images.hktv-img.com/images/HKTV/12668/ADC-ABS9L_main_36460776_20200403173306_01_1200.jpg', },
        //Aluminum Tripod
        { item_id: itemIDs[16], expiry_date: XpiredDate, quantity: 1, image: 'https://images.hktv-img.com/images/HKTV/20636/6933126200341_main_27931569_20190713173838_01_1200.jpg', },
        //Portable Folding Mini Photography Tool Box
        { item_id: itemIDs[17], expiry_date: XpiredDate, quantity: 1, image: 'https://images.hktv-img.com/images/HKTV/11405/photo-1_main_36057690_20200325011859_01_1200.jpg', },
        //2TB USB 3.0 2.5" Portable HDD
        { item_id: itemIDs[18], expiry_date: '2019-12-01', quantity: 2, image: 'https://images.hktv-img.com/images/HKTV/10757/EHDSEASTEA2000400_main_32459442_20200107131242_01_1200.jpg', },
        //AC1200 MU-MIMO Gigabit Router*Warranty Period :3 Years
        { item_id: itemIDs[19], expiry_date: '2022-09-01', quantity: 2, image: 'https://images.hktv-img.com/images/HKTV/13859/TL-ARCHER-C6_main_45306945_20200710145903_01_1200.jpg', },
        //A4 paper
        { item_id: itemIDs[20], expiry_date: XpiredDate, quantity: 10, image: 'https://images.hktvmall.com/h1888001/181087/h1888001_16002534_180508041724_01_1200.jpg', },
        //Adhesive Type (3/4" X 25yd) X 8 rolls
        { item_id: itemIDs[21], expiry_date: XpiredDate, quantity: 3, image: 'https://images.hktv-img.com/images/HKTV/15953/11105_main_38366436_20200426231129_01_1200.jpg', },
        //Color Whiteboard Marker Set 12-Pack
        { item_id: itemIDs[22], expiry_date: XpiredDate, quantity: 5, image: 'https://images.hktv-img.com/images/HKTV/10637/AWMY-2302_main_14996690_20190131120233_01_1200.jpg', },
        //4" x 9" White Envelope with narrow open 20pcs/pack
        { item_id: itemIDs[23], expiry_date: XpiredDate, quantity: 4, image: 'https://images.hktv-img.com/images/HKTV/15953/140162_main_37516344_20200419121140_01_1200.jpg', },
        //MX Vertical Mouse*Warranty Period : 1 Year
        { item_id: itemIDs[24], expiry_date: '2021-12-30', quantity: 8, image: 'https://images.hktv-img.com/images/HKTV/32220/910-005449_product_47759955_20200811232742_01_1200.jpg', },
        //Bluetooth Keyboard*Warranty Period : 1 Year
        { item_id: itemIDs[25], expiry_date: '2021-12-30', quantity: 8, image: 'https://images.hktv-img.com/images/HKTV/10052/097855148117_main_44932452_20200707132718_01_1200.jpg', },
        //Disinfectant Spray
        { item_id: itemIDs[26], expiry_date: '2020-12-11', quantity: 5, image: 'https://images.hktv-img.com/images/HKTV/10859/HYGINOVA_400_main_43539624_20200615013216_01_1200.jpg', },
        //Wet Wipe
        { item_id: itemIDs[27], expiry_date: '2020-12-11', quantity: 5, image: 'https://images.hktv-img.com/images/HKTV/13688/BA070001_main_36154488_20200327151519_01_1200.jpg', },
        //Bamboo Fiber Tissue
        { item_id: itemIDs[28], expiry_date: '2020-12-11', quantity: 10, image: 'https://images.hktv-img.com/images/HKTV/16667/CM-0090_main_28545171_20190802172117_01_1200.jpg', },
        //Patent mop
        { item_id: itemIDs[29], expiry_date: XpiredDate, quantity: 5, image: 'https://images.hktv-img.com/images/HKTV/18260/65670018002_main_44518365_20200630093039_01_1200.jpg', },
        //All Purpose Gloves
        { item_id: itemIDs[30], expiry_date: XpiredDate, quantity: 2, image: 'https://images.hktvmall.com/h0389001/b7a41f7e315cb59fc8cbcf417074223ddae81730/h0389001_4891203044023_160407022847_01_1200.jpg', },
        //Mamee Noodles x 10 bags
        { item_id: itemIDs[31], expiry_date: '2020-10-03', quantity: 2, image: 'https://cdn11.bigcommerce.com/s-bnlexqrw6m/images/stencil/500x500/products/178/556/__94228.1515382806.jpg', },
        //Golden Duck - SALTED EGG CRAB SEAWEED TEMPURA
        { item_id: itemIDs[32], expiry_date: '2020-11-23', quantity: 10, image: 'https://images.hktv-img.com/images/HKTV/6023/F00140_main_27914037_20190712160637_01_1200.jpg', },
        //Tomato Flavor Potato Chips x 6 packs
        { item_id: itemIDs[33], expiry_date: '2020-11-11', quantity: 3, image: 'https://images.hktv-img.com/images/HKTV/10856/LS_0721_6_main_32802159_20200119191013_01_1200.jpg', },
        //RedBull 24 罐裝
        { item_id: itemIDs[34], expiry_date: '2020-11-23', quantity: 2, image: 'https://images.hktvmall.com/h0888001/43fcaf9deb63c38bfcc0ca9c690bcf44b539df33/h0888001_10021530e_200915103721_01_1200.jpeg', },
        //Bento - Spicy Squid Seafood Snacks 5g x 6packs
        { item_id: itemIDs[35], expiry_date: '2020-10-13', quantity: 4, image: 'https://gd.image-gmkt.com/li/783/443/763443783.g_520-w_g.jpg', },
        //Calbee - [Full Case] Spicy Flavour Potato Chips 25g x 30 bags
        { item_id: itemIDs[36], expiry_date: '2020-11-23', quantity: 1, image: 'https://www.parknshop.com/medias/sys_master/front/prd/9185788919838.jpg', },
        //Calbee - [Full Case]Grill A Corn Barbecue Flavoured 32g x 30 bags
        { item_id: itemIDs[37], expiry_date: '2020-11-23', quantity: 2, image: 'https://www.parknshop.com/medias/sys_master/front/prd/8916215562270.jpg', },
        //Sze Hing Loong - Chilli Corn Roll 15g x 6 packs
        { item_id: itemIDs[38], expiry_date: '2020-11-23', quantity: 4, image: 'https://images.hktv-img.com/images/HKTV/10856/LS_02048_6_main_34771587_20200224125323_01_1200.jpg', },
        //Baby Star - Dodekai Fettuccine Chicken Flavour 37g x 24 packs
        { item_id: itemIDs[39], expiry_date: '2020-10-29', quantity: 1, image: 'https://images.hktv-img.com/images/HKTV/15953/400120_main_38108451_20200423194924_01_1200.jpg', },
        //Taiyo - Wiener Sausage 22g x 10 packs
        { item_id: itemIDs[40], expiry_date: '2020-12-02', quantity: 6, image: 'https://www.parknshop.com/medias/sys_master/front/prd/9145383354398.jpg', },
        //Pretz Salad 9pcs x2 packs
        { item_id: itemIDs[41], expiry_date: '2020-10-03', quantity: 3, image: 'https://images.hktvmall.com/h0888001/97f33cda2febb90f4b1a8b5a720214549b5f4053/h0888001_10014585a_191025044809_01_1200.jpg', },
        //Strawberry Pocky 9pcs x2 packs
        { item_id: itemIDs[42], expiry_date: '2020-11-23', quantity: 3, image: 'https://images.hktvmall.com/h0888001/244558/h0888001_10014584a_190317110816_01_1200.jpg', },
        //Tomato Pretz 9pcs x2 packs
        { item_id: itemIDs[43], expiry_date: '2020-10-30', quantity: 3, image: 'https://images.hktvmall.com/h0888001/7aa26ab8918c1fbe92b8ab6904be38c521d1901e/h0888001_10014586a_191025045331_01_1200.jpg', },
        //Calbee - Vegetables Fries
        { item_id: itemIDs[44], expiry_date: '2020-11-12', quantity: 8, image: 'https://images.hktvmall.com/h0888001/8cd63b8c00e93df48bb5787c297cc0959f159bb0/h0888001_10011259b_180924122908_01_1200.jpg', },
        //Lotte - Koala March's Chocolate Biscuits Family Pack x2
        { item_id: itemIDs[45], expiry_date: '2020-11-23', quantity: 2, image: 'https://images.hktvmall.com/h0888001/248190/h0888001_10011296a_190627101429_01_1200.jpg', },
        //Haribo - Goldbaeren Gummy Mini x2
        { item_id: itemIDs[46], expiry_date: '2020-10-23', quantity: 2, image: 'https://images.hktvmall.com/h0888001/248856/h0888001_10134339a_190712023342_01_1200.jpg', },
        //Nabisco - Chips Ahoy Chocolate Chip Cookies 21g x 12 Packs
        { item_id: itemIDs[47], expiry_date: '2020-10-15', quantity: 2, image: 'https://images.hktvmall.com/h0888001/43713/h0888001_10013817_160524060609_01_1200.jpg', },
        //Vitasoy - Malted Soyabean Milk 250ml x 9
        { item_id: itemIDs[48], expiry_date: '2020-10-14', quantity: 3, image: 'https://images.hktvmall.com/h0888001/abe2b6569025ae0f1b4511a5d68656af59fdc75b/h0888001_10003286_180306062528_01_1200.jpg', },
        //Vita - Lemon Tea 6P x 4
        { item_id: itemIDs[49], expiry_date: '2020-10-14', quantity: 3, image: 'https://www.parknshop.com/medias/sys_master/front/prd/9201432920094.jpg', },
        //Coca-Cola 330毫升 x 12
        { item_id: itemIDs[50], expiry_date: '2020-11-23', quantity: 2, image: 'https://images.hktvmall.com/h0888001/ccc94c890c629bbdca4eda301ba5c36570405777/h0888001_10022674_200616061538_01_1200.jpg', },
        //Calbee - [Full Case]Prawn Stick Original Flavour 40g x 30 
        { item_id: itemIDs[51], expiry_date: '2020-11-23', quantity: 1, image: 'https://images.hktvmall.com/h0888001/8447256b74d76e340dc25c6379c4001c4e42a330/h0888001_10011241a_191214010525_01_1200.jpg', },
        //Calbee - Pizza Flavor Potato Chips Hot & Spicy Thick Cuts Chips 55g x 24bags
        { item_id: itemIDs[52], expiry_date: '2020-11-23', quantity: 1, image: 'https://www.parknshop.com/medias/sys_master/front/prd/8885417377822.jpg', },
        //FOUR SEAS - Seaweed Crispy Prawn Crackers 15g x 40 bags
        { item_id: itemIDs[53], expiry_date: '2020-12-11', quantity: 1, image: 'https://images.hktv-img.com/images/HKTV/15953/400767K_main_48460215_20200820224917_01_1200.jpg', },
        //Korea. - High Class Snack Seaweed 15g x 30pack
        { item_id: itemIDs[54], expiry_date: '2020-11-24', quantity: 2, image: 'https://images.hktv-img.com/images/HKTV/27759/ZD467_main_49781388_20200909123310_01_1200.jpg', },
        //Calbee - [Full Case]Seaweed Flavour Chips 25g x 24
        { item_id: itemIDs[55], expiry_date: '2020-11-26', quantity: 1, image: 'https://www.zing-asia.co.uk/14038-large_default/Calbee-Snacks--Seaweed-Original-Flavoured-Potato-Chips-snack.jpg', },
        //ORIHIRO - Konjac Jelly Peach & Lychee (12pcs) X 3bag
        { item_id: itemIDs[56], expiry_date: '2020-11-23', quantity: 2, image: 'https://images.hktv-img.com/images/HKTV/1286/55903_main_27265464_20190621154809_01_1200.jpg', },
        //Tao Ti - Supreme Meta Tea (No sugar) 
        { item_id: itemIDs[57], expiry_date: '2020-12-22', quantity: 4, image: 'https://images.hktv-img.com/images/HKTV/31329/DRKTNTEA001_main_48634098_20200824185115_01_1200.jpg', },
        //CheckCheckCin - Osmanthus & Pear Rice Water
        { item_id: itemIDs[58], expiry_date: '2020-11-23', quantity: 2, image: 'https://cdn.ztore.com/images/ztore/production/product/750px/6003343_1.jpg', },
        //HealthWorks - Lemon Yiyiren Drink (250ml X 6 X 2)
        { item_id: itemIDs[59], expiry_date: '2020-11-23', quantity: 2, image: 'https://images.hktv-img.com/images/HKTV/15953/400302_main_39330330_20200506230153_01_1200.jpg', },
        //日本進口 - (Parallel) (BOX) SANGARIA Strong bottle green tea 500ML X 24
        { item_id: itemIDs[60], expiry_date: '2020-12-28', quantity: 2, image: 'https://images.japancentre.com/images/pics/9153/large/original.jpg', },
        //Lotte - Milkis Original (PET)
        { item_id: itemIDs[61], expiry_date: '2020-11-23', quantity: 4, image: 'https://images.hktvmall.com/h0888001/0a1903689ec4cf8aa8d75e4292a99cd147534728/h0888001_10006660_190801043132_01_1200.jpg', },
        //日本進口 - (Parallel) Ucc - [Full Case]Blended Coffee 185g x 30
        { item_id: itemIDs[62], expiry_date: '2020-12-25', quantity: 1, image: 'https://images.hktv-img.com/images/HKTV/28311/ucccoffee1850201_main_40554369_20200521162604_01_1200.jpg', },
        //HealthWorks - Hawthorn Apple Herbal Drink PET x 24
        { item_id: itemIDs[63], expiry_date: '2021-01-03', quantity: 1, image: 'https://images.hktvmall.com/h0888001/11d68ed8ef32f9a9cf2b84f3289dc0a3e58025ef/h0888001_10003477a_151013060852_01_1200.jpg', },
        //Oreo - Original Mini Multipack
        { item_id: itemIDs[64], expiry_date: '2021-01-01', quantity: 3, image: 'https://images.hktvmall.com/h0888001/4e7e76e90ca7d5559857fbba9a9432b1b06b2d03/h0888001_10137022a_191106033035_05_1200.jpg', },
        //Oreo - Oreo - Original Sandwich Biscuits 9s 264.6g
        { item_id: itemIDs[65], expiry_date: '2020-11-01', quantity: 3, image: 'https://images.hktv-img.com/images/HKTV/27039/SK0017_main_39321843_20200506190913_01_1200.jpg', },
        //Banana
        // { item_id: itemIDs[66], expiry_date: '2021-01-01', quantity: 1, image: '', },

    ]).returning('id');

    await knex(tables.TRX).insert([
        { item_id: itemIDs[0], inventory_id: invIDs[0], updated_quantity: 1, },//+ apple(itemIDs[0],invIDs[0])
        { item_id: itemIDs[0], inventory_id: invIDs[0], updated_quantity: 2, },//+ apple(itemIDs[0],invIDs[0])
        { item_id: itemIDs[0], inventory_id: invIDs[0], updated_quantity: 3, },//+ apple(itemIDs[0],invIDs[0])
    ]);
};
