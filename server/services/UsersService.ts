import Knex from "knex";
import { IUser } from "./models";
import { tables } from "../tables";

export class UsersService {
    constructor(private knex: Knex) { }

    getUserByUsername = async (username: string) => {
        const user = await this.knex<IUser>(tables.USERS)
            .where("username", username)
            .first();
        return user;
    };

    getUserByID = async (id: number) => {
        const user = await this.knex<IUser>(tables.USERS)
            .where("id", id)
            .first();
        return user;
    };

    createUser = async (username: string, password: string) => {
        const UserWithGroup = await this.knex.transaction();
        try {
            const [id] = await UserWithGroup(tables.USERS)
                .insert({
                    username,
                    password
                })
                .returning('id');

            const [group_id] = await UserWithGroup(tables.GROUPS)
                .insert({
                    group_name: username,
                    owner: username,
                }).returning('id');

            await UserWithGroup(tables.GPAccess)
                .insert({
                    group_id: group_id,
                    user_id: id,
                    access_right: 'owner',
                }).returning('id');
            await UserWithGroup.commit();
            return id as number;

        } catch (err) {
            await UserWithGroup.rollback();
            throw err;
        }
    }

    // getAllUsers = async () => {
    //     const allUser = await this.knex<IUser>(tables.USERS)
    //         .select(`${tables.USERS}.username`);
    //     return allUser;
    // };

    getAllUsersNotInGp = async (GpID: number) => {
        const subquery = this.knex.select('user_id').from(tables.GPAccess).where("group_id", GpID);
        const allUser = await this.knex(tables.GPAccess)
            .select(
                // `${tables.GPAccess}.group_id`,
                `${tables.GPAccess}.user_id`,
                `${tables.USERS}.username`
            )
            .distinct(`${tables.USERS}.username`)
            .innerJoin(tables.GROUPS, `${tables.GPAccess}.group_id`, `${tables.GROUPS}.id`)
            .innerJoin(tables.USERS, `${tables.GPAccess}.user_id`, `${tables.USERS}.id`)
            .whereNot('group_id', GpID)
            .where('user_id', 'not in', subquery)
            .returning('username')
            .orderBy('username', 'asc');
        return allUser;
    };

}
