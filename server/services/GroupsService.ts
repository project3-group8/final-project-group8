
import Knex from "knex";
import { tables } from "../tables";
import { Igp_access } from "./models";

export class GroupsService {
    constructor(private knex: Knex) { }

    getGroups = async (userID: number) => {
        const subquery = this.knex.select('group_id').from(tables.GPAccess).where("user_id", userID);
        const userInGp = this.knex<Igp_access>(tables.GPAccess)
            .select(
                `${tables.GPAccess}.group_id`,
                `${tables.GROUPS}.group_name`,
                `${tables.GPAccess}.user_id`,
                `${tables.USERS}.username`,
                `${tables.GPAccess}.access_right`,
                `${tables.GROUPS}.owner`,
                `${tables.GROUPS}.invitation_code`,
            )
            .innerJoin(tables.GROUPS, `${tables.GPAccess}.group_id`, `${tables.GROUPS}.id`)
            .innerJoin(tables.USERS, `${tables.GPAccess}.user_id`, `${tables.USERS}.id`)
            .whereIn('group_id', subquery);
        // console.log(userInGp.toSQL())
        return await userInGp;
    };

    getGroupInfoMembersByGpID = async (GpID: number) => {
        const GpInfoMembers = await this.knex<Igp_access>(tables.GPAccess)
            .select(
                `${tables.GPAccess}.group_id`,
                `${tables.GROUPS}.group_name`,
                `${tables.GPAccess}.user_id`,
                `${tables.USERS}.username`,
                `${tables.GPAccess}.access_right`,
                `${tables.GROUPS}.owner`,
                `${tables.GROUPS}.invitation_code`,
            )
            .innerJoin(tables.GROUPS, `${tables.GPAccess}.group_id`, `${tables.GROUPS}.id`)
            .innerJoin(tables.USERS, `${tables.GPAccess}.user_id`, `${tables.USERS}.id`)
            .where("group_id", GpID);
        // .toSQL();
        // console.log(`${GpInfoMembers}from getGroupInfoMembersByGpID---services`);
        // console.log(`${GpInfoMembers}from getGroupInfoMembersByGpID---services`);
        return GpInfoMembers;
    };



    // getGroupsByUserID = async (userID: number) => {
    //     const userInGp = await this.knex<Igp_access>(tables.GPAccess)
    //         .select(
    //             `${tables.GPAccess}.user_id`,
    //             `${tables.GPAccess}.group_id`,
    //             `${tables.GROUPS}.group_name`,
    //     // `${tables.GPAccess}.access_right`,
    //     // `${tables.GROUPS}.owner`,
    //     // `${tables.GROUPS}.invitation_code`,
    //         )
    //         .innerJoin(tables.GROUPS, `${tables.GPAccess}.group_id`, `${tables.GROUPS}.id`)
    //         .where("user_id", userID);
    //     return userInGp;
    // };

    // getGroupInfoByGpID = async (GpID: number) => {
    //     const GpDetail = await this.knex<IGroup>(tables.GROUPS)
    //         .select('*')
    //         .where("id", GpID);
    //     // console.log(`${GpDetail}from getGroupInfoByGpIDservices`);
    //     // console.log(`${GpID}from getGroupInfoByGpIDservices`);
    //     return GpDetail;
    // };


    // getGpMembersByGpID = async (GpID: number) => {
    //     const GpMembers = await this.knex<Igp_access>(tables.GPAccess)
    //         .select(
    //             `${tables.GPAccess}.group_id`,
    //             `${tables.GROUPS}.group_name`,
    //             `${tables.GPAccess}.user_id`,
    //             `${tables.USERS}.username`,
    //             `${tables.GPAccess}.access_right`,
    //     //   `${tables.GROUPS}.owner`,
    //     //   `${tables.GROUPS}.invitation_code`,
    //         )
    //         .innerJoin(tables.GROUPS, `${tables.GPAccess}.group_id`, `${tables.GROUPS}.id`)
    //         .innerJoin(tables.USERS, `${tables.GPAccess}.user_id`, `${tables.USERS}.id`)
    //         .where("group_id", GpID);
    //     // .toSQL();
    //     // console.log(`${GpMembers}from getGpMembersByGpIDservices`);
    //     // console.log(`${GpID}from getGpMembersByGpIDservices`);
    //     return GpMembers;
    // };

    createGroup = async (
        user_id: number,
        existingUser: string,
        newGroupName: string,
        GPtoken: string
    ) => {
        // console.log(`from ${user_id}`);
        // console.log(`from ${existingUser}`);
        // console.log(`from ${newGroupName}`);
        const { count } = (await this.knex(tables.GROUPS)
            .select(`${tables.GROUPS}.*`)
            .innerJoin(tables.GPAccess, `${tables.GROUPS}.id`, `${tables.GPAccess}.group_id`)
            .where(`${tables.GROUPS}.group_name`, '!=', newGroupName)
            .andWhere(`${tables.GROUPS}.owner`, '!=', existingUser)
            .first()) as { count: string };
        const existing_group = parseInt(count);
        existing_group > 0;
        // console.log(existing_group);
        // console.log(`${JSON.stringify(count)} is existing`);

        if (!existing_group) {
            const newGroupWithAccess = await this.knex.transaction();
            try {
                // console.log(`${newGroupWithAccess} is work!`)
                const [groupID] = await newGroupWithAccess(tables.GROUPS)
                    .insert({
                        group_name: newGroupName,
                        owner: existingUser,
                        invitation_code: GPtoken
                    }).returning('id');
                console.log(`New group ${groupID} ${newGroupName} is created`);

                const [GPAccessID] = await newGroupWithAccess(tables.GPAccess)
                    .insert({
                        group_id: groupID,
                        user_id: user_id,
                        access_right: 'owner',
                    }).returning('id');
                console.log(groupID, GPAccessID)
                await newGroupWithAccess.commit();
                // console.log(`${groupID}${GPAccessID} at createGroup groupsService`)
                return groupID as number;
            } catch (err) {
                await newGroupWithAccess.rollback();
                throw err;
            }
        }
        return;
    }


    addUsersNotInGp = async (
        GpID: number,
        addMembersArr: number[],
        // access_right: string
    ) => {
        const insertArr = addMembersArr.map((addMember: any) => (
            {
                group_id: GpID,
                user_id: addMember.user_id,
                access_right: 'reader'
            }
        ))
        const addUsers = await this.knex(tables.GPAccess)
            .insert(
                insertArr
            ).returning('id');
        // console.log(addUsers);
        return addUsers;
    }

    removeUsersInGp = async (
        GpID: number,
        removeMember: number
    ) => {
        const removeUsers = await this.knex(tables.GPAccess)
            .select('*')
            .where('group_id', GpID)
            .andWhere('user_id', removeMember)
            .del()
            .returning('id');
        // console.log(removeUsers);
        return removeUsers;
    }

    getMemberAccessByGpID = async (UserID: number, GpID: number) => {
        const memberAccess = await this.knex<Igp_access>(tables.GPAccess)
            .select(
                `${tables.GPAccess}.group_id`,
                `${tables.GPAccess}.user_id`,
                `${tables.GPAccess}.access_right`,
            )
            .where("group_id", GpID)
            .andWhere('user_id', UserID)
            .returning('access_right');
        // .toSQL();
        // console.log(`${memberAccess}from getMemberAccessByGpID---services`);
        return memberAccess;
    };

    updateMemberAccessInGp = async (
        GpID: number,
        updateMemberID: number,
        updateAccess: string
    ) => {
        const updateMember = await this.knex(tables.GPAccess)
            .select(
                `${tables.GPAccess}.group_id`,
                `${tables.GPAccess}.user_id`,
                `${tables.GPAccess}.access_right`,
            )
            .where('group_id', GpID)
            .andWhere('user_id', updateMemberID)
            .update('access_right', updateAccess)
            .returning('access_right');
        // .toSQL();
        // console.log(updateMember);
        // console.log(`${updateMember}from updateMemberAccessInGp---services`);
        return updateMember;
    }

    getDefaultGroupByUsername = async (username: string) => {
        const group = await this.knex(tables.GROUPS)
            .select(`${tables.GROUPS}.group_name`,
                `${tables.GROUPS}.id as group_id`
            )
            .where('owner', username)
            .orderBy(`${tables.GROUPS}.id`, 'asc')
            .first();

        return group;

    }

}
