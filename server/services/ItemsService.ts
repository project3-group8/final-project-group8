import Knex from "knex";
import { tables } from "../tables";

export class ItemsService {
    constructor(private knex: Knex) { }


    getListItemsByUserID = async (userID: number) => {

        const user = await this.knex(tables.USERS)
            .where("id", userID)
            .first();

        const group = await this.knex(tables.GROUPS)
            .select(`${tables.GROUPS}.group_name`,
                `${tables.GROUPS}.id as group_id`
            )
            .where('owner', user.username)
            .orderBy(`${tables.GROUPS}.id`, 'asc')
            .first();

        const data = await this.knex(tables.ITEMS)
            .select(`${tables.ITEMS}.id as item_id`,
                `${tables.ITEMS}.product`,
                `${tables.ITEMS}.category`,
                `${tables.ITEMS}.user_id`,
                `${tables.ITEMS}.group_id`,
                `${tables.INV}.expiry_date`,
                `${tables.INV}.quantity`,
                `${tables.INV}.image`,
                `${tables.INV}.id as inventory_id`
            )
            .innerJoin(tables.INV, `${tables.ITEMS}.id`, `${tables.INV}.item_id`)
            // .where(`${tables.ITEMS}.user_id`, userID)
            .where(`${tables.ITEMS}.group_id`, group.group_id)
            .andWhere(`${tables.INV}.expiry_date`, '>=', 'now()')
            .orderBy(`${tables.INV}.expiry_date`, 'asc');
        console.log(data);
        return data;
    };


    getChartItemsByGroup = async (group_id: number) => {
        const data = await this.knex(tables.ITEMS)
            .select(`${tables.ITEMS}.id as item_id`,
                `${tables.ITEMS}.product`,
                `${tables.ITEMS}.category`//,
                // `${tables.ITEMS}.user_id`,
                // `${tables.ITEMS}.group_id`,
                // `${tables.INV}.expiry_date`,
                // `${tables.INV}.quantity`,
                // `${tables.INV}.image`,
                // `${tables.INV}.id as inventory_id`
            )
            .sum(`${tables.INV}.quantity`)
            .innerJoin(tables.INV, `${tables.ITEMS}.id`, `${tables.INV}.item_id`)
            .where(`${tables.ITEMS}.group_id`, group_id)
            // .andWhere(`${tables.ITEMS}.user_id`, user_id)
            // .andWhere(`${tables.ITEMS}.category`, category)
            .andWhere(`${tables.INV}.expiry_date`, '>=', 'now()')
            .groupBy(`${tables.ITEMS}.id`)
            .groupBy(`${tables.ITEMS}.product`)
            .groupBy(`${tables.ITEMS}.category`);
        // .havingRaw(`sum(${tables.INV}.quantity) <= ${alert_quantity}`);
        // .orderBy(`${tables.INV}.expiry_date`, 'asc');
        console.log("data from itemsService.getChartItemsByGroup:", data);
        return data;
    }


    getChartItemsByGroupByCat = async (group_id: number, category: string) => {

        let data;
        if (category == 'All') {
            data = await this.knex(tables.ITEMS)
                .select(`${tables.ITEMS}.id as item_id`,
                    `${tables.ITEMS}.product`,
                    `${tables.ITEMS}.category`//,
                    // `${tables.ITEMS}.user_id`,
                    // `${tables.ITEMS}.group_id`,
                    // `${tables.INV}.expiry_date`,
                    // `${tables.INV}.quantity`,
                    // `${tables.INV}.image`,
                    // `${tables.INV}.id as inventory_id`
                )
                .sum(`${tables.INV}.quantity`)
                .innerJoin(tables.INV, `${tables.ITEMS}.id`, `${tables.INV}.item_id`)
                .where(`${tables.ITEMS}.group_id`, group_id)
                // .andWhere(`${tables.ITEMS}.user_id`, user_id)
                // .andWhere(`${tables.ITEMS}.category`, category)
                .andWhere(`${tables.INV}.expiry_date`, '>=', 'now()')
                .groupBy(`${tables.ITEMS}.id`)
                .groupBy(`${tables.ITEMS}.product`)
                .groupBy(`${tables.ITEMS}.category`);
            // .havingRaw(`sum(${tables.INV}.quantity) <= ${alert_quantity}`);
            // .orderBy(`${tables.INV}.expiry_date`, 'asc');
        } else {
            data = await this.knex(tables.ITEMS)
                .select(`${tables.ITEMS}.id as item_id`,
                    `${tables.ITEMS}.product`,
                    `${tables.ITEMS}.category`//,
                    // `${tables.ITEMS}.user_id`,
                    // `${tables.ITEMS}.group_id`,
                    // `${tables.INV}.expiry_date`,
                    // `${tables.INV}.quantity`,
                    // `${tables.INV}.image`,
                    // `${tables.INV}.id as inventory_id`
                )
                .sum(`${tables.INV}.quantity`)
                .innerJoin(tables.INV, `${tables.ITEMS}.id`, `${tables.INV}.item_id`)
                .where(`${tables.ITEMS}.group_id`, group_id)
                // .andWhere(`${tables.ITEMS}.user_id`, user_id)
                .andWhere(`${tables.ITEMS}.category`, category)
                .andWhere(`${tables.INV}.expiry_date`, '>=', 'now()')
                .groupBy(`${tables.ITEMS}.id`)
                .groupBy(`${tables.ITEMS}.product`)
                .groupBy(`${tables.ITEMS}.category`);
            // .havingRaw(`sum(${tables.INV}.quantity) <= ${alert_quantity}`);
            // .orderBy(`${tables.INV}.expiry_date`, 'asc');
        }
        console.log("data from itemsService.getChartItemsByGroupByCat:", data);
        return data;
    }


    getExpiryItemsByGroup = async (group_id: number, day_before: number) => {
        const data = await this.knex(tables.ITEMS)
            .select(`${tables.ITEMS}.id as item_id`,
                `${tables.ITEMS}.product`,
                `${tables.ITEMS}.category`,
                `${tables.INV}.expiry_date`,
                `${tables.INV}.quantity`
            )
            .innerJoin(tables.INV, `${tables.ITEMS}.id`, `${tables.INV}.item_id`)
            .whereRaw(`${tables.INV}.expiry_date - ${day_before} <= current_date`)
            .andWhere(`${tables.INV}.expiry_date`, '>=', 'now()')
            .andWhere(`${tables.ITEMS}.group_id`, group_id)
            .orderBy(`${tables.INV}.expiry_date`, 'asc');
        console.log("data from itemsService.getExpiryItemsByGroup:", data);
        return data;
    }


    getShortageItemsByGroup = async (group_id: number, alert_quantity: number) => {
        const data = await this.knex(tables.ITEMS)
            .select(`${tables.ITEMS}.id as item_id`,
                `${tables.ITEMS}.product`,
                `${tables.ITEMS}.category`//,
                // `${tables.ITEMS}.user_id`,
                // `${tables.ITEMS}.group_id`,
                // `${tables.INV}.expiry_date`,
                // `${tables.INV}.quantity`,
                // `${tables.INV}.image`,
                // `${tables.INV}.id as inventory_id`
            )
            .sum(`${tables.INV}.quantity`)
            .innerJoin(tables.INV, `${tables.ITEMS}.id`, `${tables.INV}.item_id`)
            .where(`${tables.ITEMS}.group_id`, group_id)
            // .andWhere(`${tables.ITEMS}.user_id`, user_id)
            // .andWhere(`${tables.ITEMS}.category`, category)
            .andWhere(`${tables.INV}.expiry_date`, '>=', 'now()')
            .groupBy(`${tables.ITEMS}.id`)
            .groupBy(`${tables.ITEMS}.product`)
            .groupBy(`${tables.ITEMS}.category`)
            .havingRaw(`sum(${tables.INV}.quantity) <= ${alert_quantity}`);
        // .orderBy(`${tables.INV}.expiry_date`, 'asc');
        console.log("data from itemsService.getShortageItemsByGroup:", data);
        return data;
    }

    getSearchItemsByGroup = async (group_id: number, key_word: string) => {
        const data = await this.knex(tables.ITEMS)
            .select(`${tables.ITEMS}.id as item_id`,
                `${tables.ITEMS}.product`,
                `${tables.ITEMS}.category`,
                `${tables.ITEMS}.user_id`,
                `${tables.ITEMS}.group_id`,
                `${tables.INV}.expiry_date`,
                `${tables.INV}.quantity`,
                `${tables.INV}.image`,
                `${tables.INV}.id as inventory_id`
            )
            .innerJoin(tables.INV, `${tables.ITEMS}.id`, `${tables.INV}.item_id`)
            .where(`${tables.ITEMS}.group_id`, group_id)
            // .andWhere(`${tables.ITEMS}.user_id`, user_id)
            .andWhereRaw(`LOWER(${tables.ITEMS}.product) like LOWER('%${key_word}%')`)
            // .andWhere(`${tables.ITEMS}.category`, category)
            .andWhere(`${tables.INV}.expiry_date`, '>=', 'now()')
            .orderBy(`${tables.INV}.expiry_date`, 'asc');

        console.log("data from itemsService.getSearchItemsByGroup:", data);
        return data;

    }


    getSearchItemsByGroupByCat = async (group_id: number, key_word: string, category: string) => {

        let data;
        if (category == 'All') {
            data = await this.knex(tables.ITEMS)
                .select(`${tables.ITEMS}.id as item_id`,
                    `${tables.ITEMS}.product`,
                    `${tables.ITEMS}.category`,
                    `${tables.ITEMS}.user_id`,
                    `${tables.ITEMS}.group_id`,
                    `${tables.INV}.expiry_date`,
                    `${tables.INV}.quantity`,
                    `${tables.INV}.image`,
                    `${tables.INV}.id as inventory_id`
                )
                .innerJoin(tables.INV, `${tables.ITEMS}.id`, `${tables.INV}.item_id`)
                .where(`${tables.ITEMS}.group_id`, group_id)
                // .andWhere(`${tables.ITEMS}.user_id`, user_id)
                .andWhereRaw(`LOWER(${tables.ITEMS}.product) like LOWER('%${key_word}%')`)
                // .andWhere(`${tables.ITEMS}.category`, category)
                .andWhere(`${tables.INV}.expiry_date`, '>=', 'now()')
                .orderBy(`${tables.INV}.expiry_date`, 'asc');

        } else {
            data = await this.knex(tables.ITEMS)
                .select(`${tables.ITEMS}.id as item_id`,
                    `${tables.ITEMS}.product`,
                    `${tables.ITEMS}.category`,
                    `${tables.ITEMS}.user_id`,
                    `${tables.ITEMS}.group_id`,
                    `${tables.INV}.expiry_date`,
                    `${tables.INV}.quantity`,
                    `${tables.INV}.image`,
                    `${tables.INV}.id as inventory_id`
                )
                .innerJoin(tables.INV, `${tables.ITEMS}.id`, `${tables.INV}.item_id`)
                .where(`${tables.ITEMS}.group_id`, group_id)
                // .andWhere(`${tables.ITEMS}.user_id`, user_id)
                .andWhereRaw(`LOWER(${tables.ITEMS}.product) like LOWER('%${key_word}%')`)
                .andWhere(`${tables.ITEMS}.category`, category)
                .andWhere(`${tables.INV}.expiry_date`, '>=', 'now()')
                .orderBy(`${tables.INV}.expiry_date`, 'asc');
        }

        console.log("data from itemsService.getSearchItemsByGroupByCat:", data);
        return data;

    }



    getSearchItemsByUser = async (user_id: number, key_word: string) => {
        const data = await this.knex(tables.ITEMS)
            .select(`${tables.ITEMS}.id as item_id`,
                `${tables.ITEMS}.product`,
                `${tables.ITEMS}.category`,
                `${tables.ITEMS}.user_id`,
                `${tables.ITEMS}.group_id`,
                `${tables.INV}.expiry_date`,
                `${tables.INV}.quantity`,
                `${tables.INV}.image`,
                `${tables.INV}.id as inventory_id`
            )
            .innerJoin(tables.INV, `${tables.ITEMS}.id`, `${tables.INV}.item_id`)
            // .where(`${tables.ITEMS}.group_id`, group_id)
            .where(`${tables.ITEMS}.user_id`, user_id)
            .andWhereRaw(`LOWER(${tables.ITEMS}.product) like LOWER('%${key_word}%')`)
            // .andWhere(`${tables.ITEMS}.category`, category)
            .andWhere(`${tables.INV}.expiry_date`, '>=', 'now()')
            .orderBy(`${tables.INV}.expiry_date`, 'asc');

        console.log("data from itemsService.getSearchItemsByUser:", data);
        return data;

    }

    getListItemsByUserByGroupByCat = async (user_id: number, group_id: number, category: string) => {
        let data;

        if (category == 'All') {
            data = await this.knex(tables.ITEMS)
                .select(`${tables.ITEMS}.id as item_id`,
                    `${tables.ITEMS}.product`,
                    `${tables.ITEMS}.category`,
                    `${tables.ITEMS}.user_id`,
                    `${tables.ITEMS}.group_id`,
                    `${tables.INV}.expiry_date`,
                    `${tables.INV}.quantity`,
                    `${tables.INV}.image`,
                    `${tables.INV}.id as inventory_id`
                )
                .innerJoin(tables.INV, `${tables.ITEMS}.id`, `${tables.INV}.item_id`)
                .where(`${tables.ITEMS}.group_id`, group_id)
                // .andWhere(`${tables.ITEMS}.user_id`, user_id)
                // .andWhere(`${tables.ITEMS}.category`, category)
                .andWhere(`${tables.INV}.expiry_date`, '>=', 'now()')
                .orderBy(`${tables.INV}.expiry_date`, 'asc');

        } else {
            data = await this.knex(tables.ITEMS)
                .select(`${tables.ITEMS}.id as item_id`,
                    `${tables.ITEMS}.product`,
                    `${tables.ITEMS}.category`,
                    `${tables.ITEMS}.user_id`,
                    `${tables.ITEMS}.group_id`,
                    `${tables.INV}.expiry_date`,
                    `${tables.INV}.quantity`,
                    `${tables.INV}.image`,
                    `${tables.INV}.id as inventory_id`
                )
                .innerJoin(tables.INV, `${tables.ITEMS}.id`, `${tables.INV}.item_id`)
                .where(`${tables.ITEMS}.group_id`, group_id)
                // .andWhere(`${tables.ITEMS}.user_id`, user_id)
                .andWhere(`${tables.ITEMS}.category`, category)
                .andWhere(`${tables.INV}.expiry_date`, '>=', 'now()')
                .orderBy(`${tables.INV}.expiry_date`, 'asc');
        }

        console.log("data from itemsService.getListItemsByUserByGroupByCat:", data);
        return data;
    };

    getCategoriesByUserByGroup = async (user_id: number, group_id: number) => {
        const categories = await this.knex(tables.ITEMS)
            .select(`${tables.ITEMS}.category`)
            .distinct()
            .where(`${tables.ITEMS}.group_id`, group_id)
        // .andWhere(`${tables.ITEMS}.user_id`, user_id);

        console.log("Categories from getCategoriesByUserByGroup:", categories);
        return categories;
    };

    createItemAndInvAndTrx = async (
        product: string,
        category: string,
        expiry_date: Date,
        quantity: number,
        image: string,
        user_id: number,
        group_id: number) => {

        // check for existing item for particular product and category
        const existing_item = await this.knex(tables.ITEMS)
            .select(`${tables.ITEMS}.id as item_id`)
            // .innerJoin(tables.INV, `${tables.ITEMS}.id`, `${tables.INV}.item_id`)
            .where(`${tables.ITEMS}.group_id`, group_id)
            // .andWhere(`${tables.ITEMS}.user_id`, user_id)
            .andWhere(`${tables.ITEMS}.product`, product)
            .andWhere(`${tables.ITEMS}.category`, category)
            .first();
        console.log("Existing_item (check before create item):", existing_item);

        let item_id;
        if (!existing_item) {
            const [new_item_id] = await this.knex(tables.ITEMS).insert({
                product: product,
                category: category,
                user_id: user_id,
                group_id: group_id,
            }).returning('id');     // return the item_id
            item_id = new_item_id;
            console.log("New item_id", item_id);
        } else {
            item_id = existing_item.item_id;
            console.log("Existing item_id", item_id);
        }
        // console.log(expiry_date, `received`)
        const [inventory_id] = await this.knex(tables.INV).insert({
            item_id: item_id,
            expiry_date: expiry_date,
            quantity: quantity,
            image: image,
        }).returning('id');     // return the inventory id
        console.log("inventory_id", inventory_id);

        const [transaction_id] = await this.knex(tables.TRX).insert({
            item_id: item_id,
            inventory_id: inventory_id,
            updated_quantity: quantity
        }).returning('id');
        console.log("transaction_id", transaction_id);

        return ({ item_id, inventory_id, transaction_id });
    }



    deleteItemAndInvAndTrx = async (
        item_id: number,
        inventory_id: number) => {

        let message;

        await this.knex.transaction(async (trx) => {

            try {
                await trx(tables.TRX)
                    .where('inventory_id', inventory_id)
                    .del();

                await trx(tables.INV)
                    .where('id', inventory_id)
                    .del();

                const inventory = await trx(tables.INV)
                    .select('id')
                    .where('item_id', item_id)
                    .first();

                if (!inventory) {
                    await trx(tables.ITEMS)
                        .where('id', item_id)
                        .del();
                }
                message = "Delete success";
            } catch (err) {
                console.log("Caught error:", err.message);
                message = "Delete fail";
            }
        });
        return message;
    }



    updateQtyItemAndInvAndTrx = async (
        item_id: number,
        inventory_id: number,
        quantity: number,
        updated_quantity: number) => {

        let message;

        await this.knex.transaction(async (trx) => {

            try {
                await trx(tables.INV)
                    .where('id', inventory_id)
                    .update('quantity', quantity);

                await trx(tables.TRX).insert({
                    item_id: item_id,
                    inventory_id: inventory_id,
                    updated_quantity: updated_quantity,
                });
                message = "Update quantity success";

            } catch (err) {
                console.log("Caught error:", err.message);
                message = "Update quantity fail";
            }
        });
        return message;
    }


}