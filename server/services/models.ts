declare global {
    namespace Express {
        interface Request {
            user?: {
                id: number;
                username: string;
            };
        }
    }
}

export interface IUser {
    id: number;
    username: string;
    password: string;
}

export interface IGroup {
    id: number;
    group_name: string;
    owner: string;
}

export interface Igp_access {
    id: number;
    group_id: number;
    user_id: number;
    access_right: string;
}