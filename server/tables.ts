export const tables = Object.freeze({
    USERS: "users",
    GROUPS: "groups",
    GPAccess: "gp_access",
    ITEMS: 'items',
    INV: 'inventory',
    TRX: 'transaction',
});
